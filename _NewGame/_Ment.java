﻿package _NewGame;

import java.util.Random;

public class _Ment extends YourData {
	
	
	
	String OpMsg[] = {
			"이젠 프로그래밍의 시대야!",
			"프로그래머를 키워서 엔딩을 보자!",
			"일단 캐릭터를 키우기 위해선 생성이 필요해!",
			"캐릭터 생성으로 넘어갈게~"
			};
	
	String ErrMsg[] = {
			"이 배열은 세지 마십시오.",
			"숫자만 입력해주세요!",
			"다시 입력해주세요!",
			"정년은 60세입니다... 60세 이하로 입력해줘",
			"17세 이상부터 만들 수 있습니다."
			};	
	
	String FristTutorial[] = {
			"~~초기 설정 안내~~",
			"메뉴를 선택해서 일정을 잡을 수 있고",
			"현재 정보에서는 내 경험치를 볼 수 있습니다!",
			"아이템샵에선 다양한 아이템으로 경험치를 더 빨리 늘릴 수 있습니다.",
			"또한 하루에 한번 씩 모든 프로그래밍 능력치가 3점씩 감소합니다.",
			"당신이 설정한 나이에 따라, 자금, 대출, 실력등이 영향을 받을 수 있습니다",
			"그럼 즐거운 게임 되세요^^"
			};
	
	public void opMentConsol() {
		for(int i=0; i<OpMsg.length; i++)
			_SetPrint.MSG(OpMsg[i],600);
	}
	
	public String opMentWindow(int i) {
		return OpMsg[i];
	}
	
	
	public void opErrMsg(int i) {
		_SetPrint.MSG(ErrMsg[i],600);
	}
	
	public String opErrMsgWindow(int i) {
		return ErrMsg[i];
	}
	
	
	
	public void FristTutorialMsg(int i) {
		_SetPrint.MSG(FristTutorial[i],600);
	}
	
	public String FristTutorialMsgWindow(int i) {
		return FristTutorial[i];
	}	
	
	
	public static String helloMessage() {
		Random hemsg = new Random();
		String str = "";
		
		if (getHappy() >= 160) {

			// 행복도 160이상
			String[] hmessage = {"요즘 너무 행복해", 
								"코딩을 한다는건 너무 즐거운 일이야", 
								"오늘도 코딩 내일도 코딩", 
								"나는야 코딩이 좋아",
								"코딩을 해서 하루하루가 행복해",
								"오늘은 정말 행복한 날이야",
								"요새 행복한 하루가 계속되서 가끔 걱정이야", 
								"이 행복이 끝까지 갔으면 좋겠다", 
								"난 행복해! 넌 어때?",
								"나는 정말 좋아!",
								"기쁜하루가 계속되서 너무 좋아"};
			for (int i=0; i<1; i++) {
				//_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				str = hmessage[hemsg.nextInt(11)];
			}
				System.out.println();
				return str;
				
		} else if (getHappy() >= 40) {
			
			// 행복도 40이상
			String[] hmessage = {"오늘도 열심히 배워보자", 
								"오늘 하루도 즐겁게 배울 수 있을거야", 
								"난 잘 배우고 있어! 너도 그렇지?", 
								"난 잘 할 수 있을거 같은 기분이 들어",
								"행복한 하루를 보낼 수 있을거야",
								"즐겁게 오늘 하루를 보내야지",
								"오늘은 무슨일을 할까", 
								"오늘은 어떤 즐거운 일을하지?", 
								"행복한 하루가 될꺼야!",
								"즐거운 하루가 될꺼야!",
								"오늘도 잘 배워보자"};
			for (int i=0; i<1; i++) {
				//_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				str = hmessage[hemsg.nextInt(11)];
			}
				System.out.println();
				return str;
				
		} else {
			// 그 이하
			String[] hmessage = {"코딩이 좀 더 필요해...", 
								"너무 슬퍼..", 
								"나 요새 좀 우울해", 
								"나 요즘 너무 힘든데.. 너는?",
								"그만 하고싶다..",
								"우울감이 너무 싫어",
								"코딩을 배우고 싶어..", 
								"너무 힘든 하루가 계속되는거 같아", 
								"앞으로의 일정이 두려운걸",
								"힘들다..",
								"우울해.."};
			for (int i=0; i<1; i++) {
				//_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				str = hmessage[hemsg.nextInt(11)];
			}
				System.out.println();
				return str;
		}
	}
	
	
	
	
	public static String albaMessage() {
		System.out.println("\n\n");
		Random bymsg = new Random();
		String str = "";
		String[] byessage = {"오늘 아르바이트 넘 힘들다", 
							"프로그래밍을 못배워서 아쉽다", 
							"내일은 프로그래밍 배워야지", 
							"프로그래머의 길을 걷기위해 벌어두자",
							"알바 하기 싫으다",
							"오늘은 배우진 못했지만 괜찮았어"
							};
		for (int i=0; i<1; i++)
			str = byessage[bymsg.nextInt(6)];
		System.out.println();
		return str;
	}
	
	public static String goHomeMessage() {
		System.out.println("\n\n");
		Random bymsg = new Random();
		String str = "";
		String[] byessage = {"오늘은 정말 알차게 보낸것 같아", 
							"재미난 하루였어!", 
							"정말 많은걸 배웠어!", 
							"오늘 많이 배워서 정말 좋았어",
							"알찬 하루를 보낸것 같아서 정말 좋다",
							"내일 배울게 기대되는걸",
							"오늘 정말 좋았어!", 
							"오늘은 너무나 즐거웠어", 
							"즐거워서 시간 흘러가는지 몰랐는걸!",
							"재미있었어!",
							"배우는건 정말 행복해"};
		for (int i=0; i<1; i++)
			str = byessage[bymsg.nextInt(11)];
		System.out.println();
		return str;
	}
	
	public static String byeMessage() {
		System.out.println("\n\n");
		Random bymsg = new Random();
		String str = "";
		String[] byessage = {"오늘은 정말 알차게 보낸것 같아", 
							"재미난 하루였어!", 
							"정말 많은걸 배웠어!", 
							"오늘 많이 배워서 정말 좋았어",
							"알찬 하루를 보낸것 같아서 정말 좋다",
							"내일 배울게 기대되는걸",
							"오늘 정말 좋았어!", 
							"오늘은 너무나 즐거웠어", 
							"즐거워서 시간 흘러가는지 몰랐는걸!",
							"재미있었어!",
							"배우는건 정말 행복해"};
		for (int i=0; i<1; i++)
			str = byessage[bymsg.nextInt(11)];
		System.out.println();
		return str;
	}
	
	
	
	public static String goOutMessage() {
		System.out.println("\n\n");
		Random bymsg = new Random();
		String str = "";
		String[] byessage = {"외출을 줄여야겠어", 
							"외출도 좋지만 공부도 해야겠어", 
							"하루에 외출을 너무 많이 하는것 같아", 
							"외출을 좀 더 줄이자",
							"외출은 즐겁지만, 공부도 해야겠다",
							"공부도 해야할것 같아"
							};
		for (int i=0; i<1; i++)
			str = byessage[bymsg.nextInt(6)];
		System.out.println();
		return str;
	}
	
	public static String EverygoOutMessage() {
		System.out.println("\n\n");
		Random bymsg = new Random();
		String str = "";
		String[] byessage = {"오늘 밖에서 외출만 했다니... 공부도 해야겠어", 
							"외출밖에 안했네;; 공부도 좀 해야지", 
							"공부 좀 해야겠다.. 내일", 
							"한번도 공부 안한건 좀 너무했다..",
							"공부 한번이라도 좀 해야할듯..",
							"공부좀해야겠다.."
							};
		for (int i=0; i<1; i++)
			str = byessage[bymsg.nextInt(6)];
		System.out.println();
		return str;
	}
	
	
	
	
	public static String StudyOutMsg(String Txt) {
		String[] msg = {};
		String msgd="";
		int count=0;
		
        if(count<msg.length)
        	msg[count++]=Txt;
        
        
		return msgd;
	}
	
	/*
	public static String coadingMessage(String Txt) {
		Random bymsg = new Random();
		String str = "";
		String[] coadingMsg = {"오늘은 정말 알차게 보낸것 같아", 
				"재미난 하루였어!", 
				"정말 많은걸 배웠어!", 
				"오늘 많이 배워서 정말 좋았어",
				"알찬 하루를 보낸것 같아서 정말 좋다",
				"내일 배울게 기대되는걸",
				"오늘 정말 좋았어!", 
				"오늘은 너무나 즐거웠어", 
				"즐거워서 시간 흘러가는지 몰랐는걸!",
				"재미있었어!",
				"배우는건 정말 행복해"};
		
		for (int i=0; i<1; i++)
			str = coadingMsg[bymsg.nextInt(11)];
		System.out.println();
		return str;
	}*/
	
	
	
	
	
	
}
