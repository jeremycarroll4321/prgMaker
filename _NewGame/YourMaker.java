﻿package _NewGame;

import java.util.Random;

/*




캐릭터 생성 설정



 */

public class YourMaker {
	_Ment ment = new _Ment();
	public int age,jender;
	public String name,realJender;
	
	public String getRealJender() {
		return realJender;
	}

	public void setRealJender(String realJender) {
		this.realJender = realJender;
	}

	public YourMaker() {super();}
	
	public YourMaker(int age, int jender,String name) {
		this.age=age;
		this.jender=jender;
		this.name=name;
	}
	
	
	//get,set설정
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getJender() {
		return jender;
	}
	public void setJender(int jender) {
		this.jender = jender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getInfo() {
		String str="이름 : "+getName()+"\n성별 : "+getJender() + "나이 :" + getAge();
		return str;
	}
	
	public String toString() {
		return name;
	}
	public void printAll() {
		System.out.println(this.getInfo());
	}
	
	public int ageRandMoney() {
		//나이에따른 자금 + 운
		YourData yd = new YourData();
		
		
		int ageResult = yd.getAge();
		int startpoint = 1;
		int endpoint = 2;
		
		if (ageResult < 18) {
			startpoint = 200;
			endpoint = 300;
		} else if (ageResult < 22) {
			startpoint = 400;
			endpoint = 650;
		} else if (ageResult < 29) {
			startpoint = 780;
			endpoint = 900;
		} else if (ageResult < 35) {
			startpoint = 900;
			endpoint = 1200;
		} else if (ageResult < 39) {
			startpoint = 1100;
			endpoint = 1300;
		} else if (ageResult < 49) {
			startpoint = 1300;
			endpoint = 1600;
		} else {
			startpoint = 1500;
			endpoint = 2200;
		}
		int rangepoint = endpoint - startpoint + 1;
		Random randomGenerator = new Random();
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		return randMoney;
	}
	
	
	public void inputInfo() throws MyException {
		
		int jender=0;
		
		System.out.print("이름을 입력해 봐 : ");
		String name2=ReadValue.input();
		setName(name2);
		

		while (true) {
			System.out.print("성별이 뭐니? (1:남성 / 2:여성) : ");
			
			try {
				jender=Integer.parseInt(ReadValue.input());
			} catch (NumberFormatException e) {
				ment.opErrMsg(1);
				jender = 35628908;
			} catch (Exception e) {}
			
				if (jender==1) {
					setRealJender("남자");
					break;
				} else if (jender==2) {
					setRealJender("여자");
					break;
				} else if (jender==35628908) {
					System.out.print(" ");
				} else {
					ment.opErrMsg(2);
					System.out.print(" ");
				}
		}
		
		
		while (true) {
			System.out.print("몇살이야? : ");
			
			try {
				age=Integer.parseInt(ReadValue.input());
			} catch (NumberFormatException e) {
				ment.opErrMsg(1);
				age = 35628908;
			} catch (Exception e) {}
			
				if (age>=60) {
					ment.opErrMsg(3); // 60세 이상 제작바람
					
				} else if(age<=16) { 
					ment.opErrMsg(4); // 17세 이상 제작바람
				} else if(age<=60 && age>=17) { 
					setAge(age);
					break;
				} else if (age==35628908) {
					System.out.print(" ");
				} else {
					System.out.println("다시 입력해봐");
					System.out.print(" ");
				} 
		}
		
		
		_SetPrint.MSG("\n너의 이름은 " + getName() + "이고, ", 1000);
		_SetPrint.MSG("앗! 넌 " + getRealJender() + "구나!", 1000);
		_SetPrint.MSG("그리고 " + getAge() + "살이네!", 1400);
		
		
		while(true) {
			int yesno = 0;
			_SetPrint.MSG("\n내가 말한 정보가 맞니? 맞으면 1번, 정보가 다르면 2번을 눌러!", 600);
			_SetPrint.MSGL("입력 : ", 300);
			
			try {
				yesno=Integer.parseInt(ReadValue.input());
			} catch (NumberFormatException e) {
				System.out.println("숫자만 입력해줄래??\n");
				jender = 35628908;
			} catch (Exception e) {}
			
			if (yesno==1) {
				_SetPrint.MSG("\n반가워!", 1000);

				//실행메소드
				
				//System.out.println(getName());
				YourData yd = new YourData();
				yd.BasicSet(getName(), getRealJender(), getAge());
				
				yd.setMoney(ageRandMoney());
				SelectWorkMenu sw = new SelectWorkMenu();
				
				sw.tutorial();
				sw.menuSelect();
				
				break;
			} else if (yesno==2){
				_SetPrint.MSG("\n아니구나..", 1000);
				_SetPrint.MSG("다시 입력해줘..", 1000);
				inputInfo();
				break;
			} else if (yesno==35628908) {
				
			} else {
				System.out.println("뭐래니.. 다시 입력해줄래? \n");
			}
			
		}
		
		
	}




	

	
}
