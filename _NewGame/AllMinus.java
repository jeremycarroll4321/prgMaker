﻿package _NewGame;

public class AllMinus extends YourData {
	public void AllminusData(int rel) {
		
		
		int minusData = 1;
		int minusRel = rel;
		
		if (getHtml() <= 200) {
			minusData = getHtml();
			minusData -= minusRel;
			setHtml(minusData);
		}
	
		if (getJquery() <= 200) {
			minusData = getJquery();
			minusData -= minusRel;
			setJquery(minusData);
		}
		
		
		if (getPhp() <= 200) {
			minusData = getPhp();
			minusData -= minusRel;
			setPhp(minusData);
		}
		
		
		if (getAsp() <= 200) {
			minusData = getAsp();
			minusData -= minusRel;
			setAsp(minusData);
		}
		
		if (getJava() <= 200) {
			minusData = getJava();
			minusData -= minusRel;
			setJava(minusData);
		}
		
		if (getJsp() <= 200) {
			minusData = getJsp();
			minusData -= minusRel;
			setJsp(minusData);
		}
		
		if (getClang() <= 200) {
			minusData = getClang();
			minusData -= minusRel;
			setClang(minusData);
		}
		
		if (getCpp() <= 200) {
			minusData = getCpp();
			minusData -= minusRel;
			setCpp(minusData);
		}
		
		if (getCshop() <= 200) {
			minusData = getCshop();
			minusData -= minusRel;
			setCshop(minusData);
		}
		
		if (getPython() <= 200) {
			minusData = getPython();
			minusData -= minusRel;
			setPython(minusData);
		}
		
		if (getObject_c() <= 200) {
			minusData = getObject_c();
			minusData -= minusRel;
			setObject_c(minusData);
		}
		
		if (getSwift() <= 200) {
			minusData = getSwift();
			minusData -= minusRel;
			setSwift(minusData);
		}	
			
	}
}
