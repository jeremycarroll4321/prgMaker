﻿package _NewGame;

import java.io.IOException;

public class ItemStore extends YourData {
	
	ItemSaver isv = new ItemSaver();
	private String Itemname;
	
	public ItemStore() {super();}
	public ItemStore(String name) {
		Itemname=name;
	}
	
	public void tutorials() {
		setItemFirstView(1);
		_SetPrint.Titles2("~~아이템샵 안내~~", 20);
		_SetPrint.MSG("어서오세요! 아이템샵입니다.", 800);
		_SetPrint.MSG("처음 오셨군요!", 800);
		
		_SetPrint.MSG("아이템샵을 안내해드릴까요? (1.예 / 2.아니오)", 800);
		
		while(true) {
			int yesno = 0;
			try {
				yesno = Integer.parseInt(ReadValue.input());
			} catch (NumberFormatException e) {
			} catch (Exception e) {}
			
			if (yesno == 1) {
				_SetPrint.MSG("이곳에선 다양한 아이템을 구매할 수 있습니다", 800);
				_SetPrint.MSG("아이템별로 다양한 효과가 있어서", 800);
				_SetPrint.MSG("공부 혹은 자금에 도움을 줍니다!", 800);
				_SetPrint.MSG("잘 생각해서 아이템을 사용해주세요!", 800);
				_SetPrint.MSG("그럼", 800);
				_SetPrint.MSG("즐거운 쇼핑되세요", 800);
				break;
			} else if (yesno == 2) {
				_SetPrint.MSG("알겠습니다! 즐거운 쇼핑되세요", 800);
				break;
			} else {
				_SetPrint.MSG("잘 못들었슴다?", 800);
				_SetPrint.MSG("다시 말씀해주세요 : ", 800);
			}
		}
		
	}
	
	public void message() {
		_SetPrint.MSG("어서오세요! 아이템샵 입니다.", 700);
		_SetPrint.MSG("좋은 상품이 많아요! 천천히 둘러봐주세요", 700);
	}
	
	public void selectList() {
		_SetPrint.MSG("1. 구매", 40);
		_SetPrint.MSG("2. 판매", 40);
		_SetPrint.MSG("3. 닫기", 40);
		_SetPrint.MSGL("입력 : ", 40);
	}
	
	public void goodsList() {
		_SetPrint.MSG("======판매하는 물건입니다======", 60);
		_SetPrint.MSG("1. 복권 (최소 0원 ~ 최고 10,000원 ) / -100원", 60);
		_SetPrint.MSG("2. 대출서비스 (즉시 +8,500원 증가 ) / 매일 -800원", 60);
		_SetPrint.MSG("3. 전문서적 ( 각 분야 +1 추가부여 ) / -900원 ", 60);
		_SetPrint.MSG("4. 보급형 노트북 ( 각 분야 +5 추가부여 ) / -1,400원 ", 60);
		_SetPrint.MSG("5. 최신형 노트북 ( 각 분야 +10 추가부여 ) / -2,000원 ", 60);
		_SetPrint.MSG("6. Apple 전자기기 ( Swift +30 추가부여 ) / -3,000원 ", 60);
		_SetPrint.MSG("7. 도우미 ( 각 분야 +8 추가부여 ) / -2,300원 ", 60);
		_SetPrint.MSG("7. AI 로봇 도우미 ( 각 분야 +25 추가부여 ) / -8,500원 ", 60);
		_SetPrint.MSG("8. 사귀는 사람 ( 주말마다 행복도 +30 ) / 주말마다 -1,000원 ", 60);
		_SetPrint.MSG("0. 뒤로가기 ", 60);
	}
	
	public void sellList() {
		_SetPrint.MSG("======저에게 팔 수 있는 물건입니다.======", 60);
		isv.itemLoad();
		inputSell();
		_SetPrint.MSG("0.뒤로", 60);
	}
	
	public void buyThanks() {
		_SetPrint.MSG("항상 감사합니다", 60);
	}
	public void noMoney() {
		_SetPrint.MSG("잔액이 부족합니다.", 60);

	}	
	
	public void realDaechul() {
		ItemInfo itemFo = new ItemInfo();
		_SetPrint.MSG("\n안녕하세요! " + getName() + "님! 대출은 계획적으로 받으시길 권장드립니다.", 600);
		_SetPrint.MSG("빌리시는 비용 혹은 소지한 금액 등 다양한 요소에 따라 하루마다 내야할 금액이 달라집니다.", 600);
		
		_SetPrint.MSGL("얼마를 빌리시겠습니까? : ", 600);
		int dMoney = Integer.parseInt(ReadValue.input()); //빌릴금액
		
		_SetPrint.MSG("네!금액을 계산해 드릴게요", 600);
		
		int yourMoney = getMoney(); // 내가 소지한 금액
		int ageAdv = (getAge() + 20) / 9;
		int yourMoneyDae = yourMoney * ageAdv; //대출한도
		
		int dayCheck = getAllDay() - getThisDay(); // 며칠남아 있습니까?
		
		double ijaPoint = 0.0;
		if (ageAdv <= 18) {
			ijaPoint = 1.75;
		} else if (ageAdv <= 22) {
			ijaPoint = 1.55;
		} else if (ageAdv <= 25) {
			ijaPoint = 1.4;
		} else if (ageAdv <= 28) {
			ijaPoint = 1.3;
		} else if (ageAdv <= 32) {
			ijaPoint = 1.2;
		} else if (ageAdv <= 39) {
			ijaPoint = 1.1;
		} else if (ageAdv <= 43) {
			ijaPoint = 0.8;
		} else if (ageAdv <= 45) {
			ijaPoint = 0.7;
		} else if (ageAdv <= 52) {
			ijaPoint = 0.5;
		} else {
			ijaPoint = 0.2;
		}
		
		int resultMoney = (int) (dMoney * ijaPoint); // 총 갚아야할 금액
		int dayResultMoney = resultMoney / dayCheck; // 하루에 갚아야 할 금액
		
		System.out.println();
		
		if (yourMoneyDae < dMoney) {
			_SetPrint.MSG(getName() + "님이 소지하신 금액에 비해 너무 큰 금액을 요구하셔서 대출 불가능 합니다.", 600);
			_SetPrint.MSG("고객님은 " + yourMoneyDae + "원 까지만 가능합니다. 다시 찾아주세요 감사합니다." , 600);
			//goodsList();
			//inputBuy();
		} else {
			_SetPrint.MSG(getName() + "님이 원하시는 금액은 " + dMoney + "원입니다.",600);
			_SetPrint.MSG(getAge() + "세 고객님의 이자율은 " + ijaPoint + "이므로, ",600);
			_SetPrint.MSG("고객님께서 총 갚아야 할 금액은 " + resultMoney + "원 입니다.",600);
			_SetPrint.MSG(dayCheck +  "일 동안 매일 "+ dayResultMoney + "원 씩 갚아 나가야 합니다.", 600);
		}
		
		_SetPrint.MSG("빌려드릴까요? (1.예 / 2.아니오) : ", 600);
		int yesno = Integer.parseInt(ReadValue.input());
		
		
		if (yesno == 1) {
			itemFo.i_daecul();
			isv.itemSave(itemFo);
			int money = getMoney();
			money += dMoney;
			setMoney(money);
			setDachulAllMoney(resultMoney);
			setDaechulDayMoney(dayResultMoney);
		} else {
			
		}
	}
	
	
	public void inputBuy() {
		ItemInfo itemFo = new ItemInfo();
		Lotto lts = new Lotto();
		_SetPrint.MSGL("입력 : ", 40);
		int buyNum = 0;
		buyNum = Integer.parseInt(ReadValue.input());
			switch (buyNum) {
			case 1:
				if (getMoney() >= 100) {
					lts.ev_start();
				} else {
					noMoney();
				}
				goodsList();
				inputBuy();
				//복권
				break;
			case 2:
				realDaechul();
				
				buyThanks();
				menuView();
				break;
			case 3:
				if (getMoney() >= 900) {
					itemFo.i_books();
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					noMoney();
				}
				goodsList();
				inputBuy();
				break;
					
			case 4:
				if (getMoney() >= 1400) {
					itemFo.i_note(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					noMoney();
				}
				goodsList();
				inputBuy();
				break;
			case 5:
				if (getMoney() >= 2000) {
					itemFo.i_note2(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					noMoney();
				}
				goodsList();
				inputBuy();
				break;
			case 6:		
				if (getMoney() >= 3000) {
					itemFo.i_apple(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					
				}
				goodsList();
				inputBuy();
				break;			
			case 7:		
				if (getMoney() >= 2300) {
					itemFo.i_helper(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					
				}
				goodsList();
				inputBuy();
				break;			
			case 8:		
				if (getMoney() >= 8500) {
					itemFo.i_aihelper(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					
				}
				goodsList();
				inputBuy();
				break;			
			case 9:				
				if (getMoney() >= -9999) {
					itemFo.i_friends(); 
					isv.itemSave(itemFo);
					buyThanks();
					menuView();
				} else {
					
				}
				goodsList();
				inputBuy();
				break;		
			case 0:
				menuView();
			default:
				break;
			}
	}
	
	public void inputSell() {
		_SetPrint.MSGL("삭제할 아이템 번호 입력 : ", 40);
		int sellNum=0;
		sellNum = Integer.parseInt(ReadValue.input());
		isv.itemDel(sellNum);
	}
	
	public void start() {
		_SetPrint.Titles2("~~Item Store~~", 50);
		
		if (getItemFirstView()==0) {
			tutorials();
		} else {
			message();
		}
		menuView();
	}
	
	
	public void menuView() {
		selectList();
		int menusel=0;
		while (true) {
			try {
				menusel = Integer.parseInt(ReadValue.input());
			} catch (Exception e) {};
			
			switch (menusel) {
			case 1:
				goodsList();
				inputBuy();
				break;
			case 2:
				sellList();
				break;
			case 3:
				SelectWorkMenu swm = new  SelectWorkMenu();
				swm.menuSelect();
				break;
			default:
				_SetPrint.MSG("잘 모르겠습니다. 다시 말씀해주세요", 60);
				break;
			}
		}
	}
	
}
