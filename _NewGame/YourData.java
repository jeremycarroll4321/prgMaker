package _NewGame;

public class YourData {
	//static String name,jender;
	//static int age;
	private static String name,jender,yoil;
	private static int age;
	
	private static int money=0;
	
	//��¥
	private static int thisDay=1,allDay=30,workTime,todayTutorial,albaCheck;
	


	//�ɷ�ġ
	private static int html,jquery,php,asp,java,jsp,clang,cpp,cshop,python,object_c,swift,alba,happy=100,itemFirstView,loveStory,goOutPoint,DachulAllMoney,DaechulDayMoney;
	
	//���Ƿ�
	private static int htmlMoney=80,jqueryMoney=95,phpMoney=100,aspMoney=115,javaMoney=120,jspMoney=100,clangMoney=115,cppMoney=130,cshopMoney=140,pythonMoney=160,object_cMoney=200,swiftMoney=210;
	private static String easyStudy = "Easy", MidiumStudy = "Medium", HardStudy = "Hard";

	
	public static int getDachulAllMoney() {
		return DachulAllMoney;
	}

	public static void setDachulAllMoney(int dachulAllMoney) {
		DachulAllMoney = dachulAllMoney;
	}

	public static int getDaechulDayMoney() {
		return DaechulDayMoney;
	}

	public static void setDaechulDayMoney(int daechulDayMoney) {
		DaechulDayMoney = daechulDayMoney;
	}

	public static int getGoOutPoint() {
		return goOutPoint;
	}

	public static void setGoOutPoint(int goOutPoint) {
		YourData.goOutPoint = goOutPoint;
	}

	public static String getEasyStudy() {
		return easyStudy;
	}

	public static void setEasyStudy(String easyStudy) {
		YourData.easyStudy = easyStudy;
	}

	public static String getMidiumStudy() {
		return MidiumStudy;
	}

	public static void setMidiumStudy(String midiumStudy) {
		MidiumStudy = midiumStudy;
	}

	public static String getHardStudy() {
		return HardStudy;
	}

	public static void setHardStudy(String hardStudy) {
		HardStudy = hardStudy;
	}

	public static int getHtmlMoney() {
		return htmlMoney;
	}

	public static void setHtmlMoney(int htmlMoney) {
		YourData.htmlMoney = htmlMoney;
	}

	public static int getJqueryMoney() {
		return jqueryMoney;
	}

	public static void setJqueryMoney(int jqueryMoney) {
		YourData.jqueryMoney = jqueryMoney;
	}

	public static int getPhpMoney() {
		return phpMoney;
	}

	public static void setPhpMoney(int phpMoney) {
		YourData.phpMoney = phpMoney;
	}

	public static int getAspMoney() {
		return aspMoney;
	}

	public static void setAspMoney(int aspMoney) {
		YourData.aspMoney = aspMoney;
	}

	public static int getJavaMoney() {
		return javaMoney;
	}

	public static void setJavaMoney(int javaMoney) {
		YourData.javaMoney = javaMoney;
	}

	public static int getJspMoney() {
		return jspMoney;
	}

	public static void setJspMoney(int jspMoney) {
		YourData.jspMoney = jspMoney;
	}

	public static int getClangMoney() {
		return clangMoney;
	}

	public static void setClangMoney(int clangMoney) {
		YourData.clangMoney = clangMoney;
	}

	public static int getCppMoney() {
		return cppMoney;
	}

	public static void setCppMoney(int cppMoney) {
		YourData.cppMoney = cppMoney;
	}

	public static int getCshopMoney() {
		return cshopMoney;
	}

	public static void setCshopMoney(int cshopMoney) {
		YourData.cshopMoney = cshopMoney;
	}

	public static int getPythonMoney() {
		return pythonMoney;
	}

	public static void setPythonMoney(int pythonMoney) {
		YourData.pythonMoney = pythonMoney;
	}

	public static int getObject_cMoney() {
		return object_cMoney;
	}

	public static void setObject_cMoney(int object_cMoney) {
		YourData.object_cMoney = object_cMoney;
	}

	public static int getSwiftMoney() {
		return swiftMoney;
	}

	public static void setSwiftMoney(int swiftMoney) {
		YourData.swiftMoney = swiftMoney;
	}

	public static String getYoil() {
		return yoil;
	}

	public static void setYoil(String yoil) {
		YourData.yoil = yoil;
	}

	public static int getItemFirstView() {
		return itemFirstView;
	}

	public static void setItemFirstView(int itemFirstView) {
		YourData.itemFirstView = itemFirstView;
	}

	public static int getHappy() {
		return happy;
	}

	public static void setHappy(int happy) {
		YourData.happy = happy;
	}

	public static int getTodayTutorial() {
		return todayTutorial;
	}

	public static void setTodayTutorial(int todayTutorial) {
		YourData.todayTutorial = todayTutorial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJender() {
		return jender;
	}

	public void setJender(String jender) {
		this.jender = jender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	public int getWorkTime() {
		return workTime;
	}


	public void setWorkTime(int workTime) {
		this.workTime = workTime;
	}


	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getThisDay() {
		return thisDay;
	}

	public void setThisDay(int thisDay) {
		this.thisDay = thisDay;
	}

	public int getAllDay() {
		return allDay;
	}

	public void setAllDay(int allDay) {
		this.allDay = allDay;
	}

	public int getHtml() {
		return html;
	}

	public void setHtml(int html) {
		this.html = html;
	}

	public int getJquery() {
		return jquery;
	}

	public void setJquery(int jquery) {
		this.jquery = jquery;
	}

	public int getPhp() {
		return php;
	}

	public void setPhp(int php) {
		this.php = php;
	}

	public int getAsp() {
		return asp;
	}

	public void setAsp(int asp) {
		this.asp = asp;
	}

	public int getJava() {
		return java;
	}

	public void setJava(int java) {
		this.java = java;
	}

	public int getJsp() {
		return jsp;
	}

	public void setJsp(int jsp) {
		this.jsp = jsp;
	}

	public int getClang() {
		return clang;
	}

	public void setClang(int clang) {
		this.clang = clang;
	}

	public int getCpp() {
		return cpp;
	}

	public void setCpp(int cpp) {
		this.cpp = cpp;
	}

	public int getCshop() {
		return cshop;
	}

	public void setCshop(int cshop) {
		this.cshop = cshop;
	}

	public int getPython() {
		return python;
	}

	public void setPython(int python) {
		this.python = python;
	}

	public int getObject_c() {
		return object_c;
	}

	public void setObject_c(int object_c) {
		this.object_c = object_c;
	}

	public int getSwift() {
		return swift;
	}

	public void setSwift(int swift) {
		this.swift = swift;
	}
	
	public int getAlba() {
		return alba;
	}

	public void setAlba(int alba) {
		this.alba = alba;
	}
	
	public static int getAlbaCheck() {
		return albaCheck;
	}

	public static void setAlbaCheck(int albaCheck) {
		YourData.albaCheck = albaCheck;
	}
	
	
	
	public static int getLoveStory() {
		return loveStory;
	}

	public static void setLoveStory(int loveStory) {
		YourData.loveStory = loveStory;
	}

	public void BasicSet(String _name,String _jender,int _age) {
		setName(_name);
		setJender(_jender);
		setAge(_age);	
	}
	
	

	
	
	public YourData() {
		name = getName();
		age = getAge();
		jender = getJender();
		money = getMoney();
		thisDay=getThisDay(); 
		html=getHtml();
		jquery=getJquery();
		php=getPhp();
		asp=getAsp();
		java=getJava();
		jsp=getJsp();
		clang=getClang();
		cpp=getCpp();
		cshop=getCshop();
		python=getPython();
		object_c=getObject_c();
		swift=getSwift();
	}
	
}
