﻿package _NewGame;

public class EndingCecker extends YourData {
	
		Reset rs = new Reset();
		int data[] = { getHtml(), getJquery(), getPhp(), getAsp(), getJava(), getJsp(), getClang(), getCpp(), getCshop(), getPython(), getObject_c(), getSwift() };
		String dataname[] = { "HTML/CSS", "JavaScript/Jquery", "PHP", "ASP", "JAVA", "JSP", "C", "C+", "C#", "Python", "Objective-C", "Swift"};
		
		
		public void chk() {
			if (getHappy() <= -80) { //행복도 -80... Ending
				_SetPrint.MSG("내 캐릭터는 행복하지가 않다..", 600);
				_SetPrint.Titles("Game Over", 300);
				rs.resetSet(); //값리셋
				Menu.menuSel();
				
			} else if (getMoney() < -500) { // -500원 파산
				_SetPrint.MSG("...파산했다.", 600);
				_SetPrint.MSG("대출은 계획적으로...", 600);
				_SetPrint.Titles("Game Over", 300);
				rs.resetSet(); //값리셋
				Menu.menuSel();
			} else if (getThisDay() == 30) { //30일째
				
				int max=0, min=0;
				String maxName="";
				for(int i=0;i<data.length;i++){
	                // 최대값 Max
	                if(data[i] > max){
	                      max = data[i];
	                      maxName=dataname[i];
	                }
				}
				
				_SetPrint.MSG("난 " + maxName + "("+ max + ")" +"를 가장 잘하게됐어!", 600);
				_SetPrint.MSG("이걸로 먹고 살아봐야겠다", 600);
				rs.resetSet(); //값리셋
				Menu.menuSel();
			}
			
			else {
				
			}
		}
}
