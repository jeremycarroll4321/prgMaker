﻿package _NewGame;

public class SelectToday extends YourData {

	public void tutorials() {
		
		setTodayTutorial(1);
		
		_SetPrint.Titles2("~~일정 설정 안내~~", 20);
		_SetPrint.MSG("반가워!", 800);
		_SetPrint.MSG("처음온것 같아!", 800);
		_SetPrint.MSG("조금 게임에 대해 알려줄까? (1.예 / 2.아니오)", 800);
		_SetPrint.MSG("입력 : ", 800);
		int yesno = 0;
		try {
			
			yesno = Integer.parseInt(ReadValue.input());
		} catch (NumberFormatException e) {
		} catch (Exception e) {}
		
		while(true) {
			if (yesno == 1) {
				_SetPrint.MSG("다음에 나올 프로그래머 용어들을 배울 수 있어", 800);
				_SetPrint.MSG("다만, 하루에 4개까지 배울 수 있고", 800);
				_SetPrint.MSG("프로그래밍 용어에 따라 다른걸 먼저 배워야 배울 수 있는것도 있어", 800);
				_SetPrint.MSG("또한 자금이 없으면 배우지 못하니", 800);
				_SetPrint.MSG("아르바이트도 해야해", 800);
				_SetPrint.MSG("다만, 알바는 하루종일 해야해서, 다른걸 못배우니 유의하자!", 800);
				break;
			} else if (yesno == 2) {
				_SetPrint.MSG("알겠어!", 800);
				break;
			} else {
				_SetPrint.MSG("대답을 좀 잘해줘", 800);
			}
		}
	}
	
	public void menuList() {
		System.out.println("===============");
		System.out.println("1. HTML/CSS - " + getEasyStudy() + " / " + getHtmlMoney() + "원");
		System.out.println("2. JavaScript/Jquery - " + getEasyStudy() + " / " + getJavaMoney() + "원");
		System.out.println("3. PHP - " + getMidiumStudy() + " / " + getPhpMoney() + "원");
		System.out.println("4. ASP - " + getMidiumStudy() + " / " + getAspMoney() + "원");
		System.out.println("5. JAVA - " + getHardStudy() + " / " + getJavaMoney() + "원");
		System.out.println("6. JSP - " + getMidiumStudy() + " / " + getJspMoney() + "원");
		System.out.println("7. C - " + getHardStudy() + " / " + getClangMoney() + "원");
		System.out.println("8. C++ - " + getHardStudy() + " / " + getCppMoney() + "원");
		System.out.println("9. C# - " + getHardStudy() + " / " + getCshopMoney() + "원");
		System.out.println("10. Python - " + getMidiumStudy() + " / " + getPythonMoney() + "원");
		System.out.println("11. Objective-C - " + getHardStudy() + " / " + getObject_cMoney() + "원");
		System.out.println("12. Swift - " + getHardStudy() + " / " + getSwiftMoney() + "원");
		System.out.println("13. 알바하기");
		System.out.println("14. 휴식");
		System.out.println("15. 외출");
	}
	public void menuPrint() {
		if (getThisDay() == 1 && getTodayTutorial() == 0) 
			tutorials();

		int menuSelNum = 0;
		if (getWorkTime()==0) {
			
			Sugoring sg = new Sugoring();
			sg.sugoMessage(); // 수고메세지 출력
			
			SelectWorkMenu swm = new SelectWorkMenu();
			swm.menuSelect(); //메뉴 선택으로 돌아가자
			
		} else {
			while(true) {
	
				menuList();
				System.out.println();
				_SetPrint.MSG("오늘 배울 수 있는 횟수 : " + getWorkTime() + "회", 600);
				StudyLevel sl = new StudyLevel();
				_SetPrint.MSGL("입력 : ", 600);
				
				try {
					menuSelNum=Integer.parseInt(ReadValue.input());
				} catch (NumberFormatException e) {
					System.out.println("숫자만 입력해줘\n");
					menuSelNum = 35628908;
				} catch (Exception e) {}
				
						switch (menuSelNum) {
						case 1:
							sl.html();
							break;
						case 2:
							sl.jquery();
							break;
						case 3:
							sl.php();
							break;
						case 4:
							sl.asp();
							break;
						case 5:
							sl.java();
							break;
						case 6:
							sl.jsp();
							break;
						case 7:
							sl.clnag();
							break;
						case 8:
							sl.cpp();
							break;
						case 9:
							sl.cshop();
							break;
						case 10:
							sl.python();
							break;
						case 11:
							sl.objective();
							break;
						case 12:
							sl.swift();
							break;
						case 13: //알바
							sl.alba();
							break;
						case 14: //휴식
							sl.vacation();
							break;
						case 15: //외출 
							sl.goOut();
							break;
							
						case 35628908:
							break;
						default:
							_SetPrint.MSG("제대로 입력해주세요", 300);
							break;
						}
				}
		}
		
		
		
		
	}

}
