﻿package _NewGame;

import java.util.Random;

public class RandomEvent extends YourData {

	int startpoint=0,endpoint=0, YesCh=0;
	double rangepoint = 0.0;
	Random randomGenerator = new Random();
	AllMinus am = new AllMinus();
	
	
	int getdata[] = { getHtml(), getJquery(), getPhp(), getAsp(), getJava(), getJsp(), getClang(), getCpp(), getCshop(), getPython(), getObject_c(), getSwift() };
	String dataname[] = { "HTML/CSS", "JavaScript/Jquery", "PHP", "ASP", "JAVA", "JSP", "C", "C+", "C#", "Python", "Objective-C", "Swift"};
	
	
	
	public void ViligeMent() {
		Random hemsg = new Random();
		String[] hmessage = {"외출은 무슨일이 일어날지 항상 기대되는걸!", 
				"외출을 하면 어떤일이 일어날까 두근두근!", 
				"밖에 나오면 그냥 좋아!", 
				"외출하다보면 누군가 코딩을 알려줄지도..!",
				"외출하다보면 돈을 줍게 될지도...!",
				"외출하다보면 그냥 뭔가 좋은일이 일어날지도..!",
				"외출하면 혹시 모르지! 좋은 사람 생길지도!", 
				"너무 외출하면 돌대가리가 될지도 몰라", 
				"외출은 항상 기대되는걸!",
				"그냥 할거 없을때 외출나오는게 제일 좋더라",
				"바깥의 상쾌한 공기! 너무 좋다"};
		for (int i=0; i<1; i++)
		_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
		System.out.println();
	}
	
	public void BasicEv() {
		ViligeMent();
		_SetPrint.MSG("딱히 아무일도 일어나지 않았다.", 600);
	}
	
	public void MoneyUp() {
		ViligeMent();
		startpoint = 25;
		endpoint = 300;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney / 2;
		
		_SetPrint.MSG("앗!! " + randMoney + "원 주웠다!!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈 " + randMoney + "원을 벌었습니다.", 600);
		_SetPrint.MSG("기분 " + happyPoint + "만큼 상승했습니다.", 600);
		
		int uMoney = getMoney();
		uMoney += randMoney;
		setMoney(uMoney);
		
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);
	}
	
	public void MoneyDown() {
		ViligeMent();
		startpoint = 25;
		endpoint = 300;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney / 2;
		
		_SetPrint.MSG("으헝ㅠ " + randMoney + "원 잃어버렸다!!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈 " + randMoney + "원을 잃었습니다.", 600);
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 감소했습니다.", 600);
		
		int uMoney = getMoney();
		uMoney -= randMoney;
		setMoney(uMoney);
		
		int uHappy = getHappy();
		uHappy -= happyPoint;
		setHappy(uHappy);
	}
	

	public void OldFriendsMeet() {
		ViligeMent();
		startpoint = 55;
		endpoint = 120;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney * 2;
		
		_SetPrint.MSG("오랫동안 못 본 친구를 만났다!", 600);
		_SetPrint.MSG("기분 좋게 " + randMoney + "원 사용했다!!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈 " + randMoney + "원을 사용했습니다.", 600);
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
		
		int uMoney = getMoney();
		uMoney -= randMoney;
		setMoney(uMoney);
		
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);

		am.AllminusData(1); 
		_SetPrint.MSG("모든 능력치 1씩 감소했습니다.", 600);
	}
	
	public void OldFriendsMeet2() {
		ViligeMent();
		startpoint = 35;
		endpoint = 60;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney + 27;
		
		_SetPrint.MSG("친한 친구를 만났다!", 600);
		_SetPrint.MSG("기분 좋게 " + randMoney + "원 사용했다!!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈 " + randMoney + "원을 사용했습니다.", 600);
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
		
		int uMoney = getMoney();
		uMoney -= randMoney;
		setMoney(uMoney);
		
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);

		am.AllminusData(1); 
		_SetPrint.MSG("모든 능력치 1씩 감소했습니다.", 600);
	}
	
	public void OldFriendsMeet3() {
		ViligeMent();
		startpoint = 185;
		endpoint = 300;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney / 8;
		
		_SetPrint.MSG("짠순이 친구를 만났다!", 600);
		_SetPrint.MSG("기분 나쁘게 " + randMoney + "원 사용했다!!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈 " + randMoney + "원을 사용했습니다.", 600);
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 감소했습니다", 600);
		
		int uMoney = getMoney();
		uMoney -= randMoney;
		setMoney(uMoney);
		
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);

		am.AllminusData(1); 
		_SetPrint.MSG("모든 능력치 1씩 감소했습니다.", 600);
	}
	
	public void OldFriendsMeet4() {
		ViligeMent();
		startpoint = 35;
		endpoint = 60;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney;
		
		_SetPrint.MSG("통큰 친구를 만났다!", 600);
		
		System.out.println();
		_SetPrint.MSG("돈은 사용하지 않았습니다.", 600);
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);

		am.AllminusData(1); 
		_SetPrint.MSG("모든 능력치 1씩 감소했습니다.", 600);
	}
	
	
	public void Crischan() {
		ViligeMent();
		startpoint = 30;
		endpoint = 70;
		rangepoint = endpoint - startpoint + 1;
		int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		int happyPoint = randMoney;
		
		_SetPrint.MSG("심심해서 종교활동을 해봤다!", 600);

		System.out.println();
		_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
		
		int uHappy = getHappy();
		uHappy += happyPoint;
		setHappy(uHappy);
	}
	
	

	public void LoveStory() {
		int LoverCheker = getLoveStory();
		if (LoverCheker ==  0) {
			_SetPrint.MSG("앗... 저 사람은..?", 600);
			_SetPrint.MSG("왜인지 맘에 든다", 600);
			_SetPrint.MSG("다음에도 만날거 같다", 600);
			setLoveStory(1); //러브스토리 체크
			
		} else if(LoverCheker ==  1) {
			_SetPrint.MSG("역시 또 보게됐잖아!", 600);
			_SetPrint.MSG("그냥 우연이겠지?", 600);
			_SetPrint.MSG("그래도 다음에 볼 수 있음 좋겠다", 600);
			setLoveStory(2); //러브스토리 체크
			
		} else if(LoverCheker == 2) {
			_SetPrint.MSGL("그 사람이다! 말 걸어볼까? (1.예 / 2.아니오) : ", 600);
			YesCh = Integer.parseInt(ReadValue.input());
			if (YesCh == 1) {
				_SetPrint.MSG("안녕! 내 이름은 " + getName() + "이야! 너 여기 살아?", 500);
				_SetPrint.MSG("그 친구와 즐겁게 놀았다", 600);
				setLoveStory(3);
			} else {
				setLoveStory(990);
			}
		} else if (LoverCheker == 990) {
			_SetPrint.MSG("어? 또 전에 봤던 그 사람이다!", 600);
			_SetPrint.MSGL("말 걸어 볼까? (1.예 / 2.아니오) : ", 600);
			YesCh = Integer.parseInt(ReadValue.input());
			if (YesCh == 1) {
				_SetPrint.MSG("안녕! 난 " + getName() + "이야! 너 여기 살아?", 500);
				_SetPrint.MSG("그 친구와 신나게 놀았다", 600);
				startpoint = 20;
				endpoint = 40;
				rangepoint = endpoint - startpoint + 1;
				int happyPoint = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
				System.out.println();
				_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
				int uHappy = getHappy();
				uHappy += happyPoint;
				setHappy(uHappy);
				setLoveStory(3);
			} else {
				setLoveStory(991);
			}
		} else if (LoverCheker == 991) {
			_SetPrint.MSG("어? 또 보는데! 왠지 이번에 못보면 다신 못볼거 같아", 600);
			_SetPrint.MSGL("말 걸어 볼까? (1.예 / 2.아니오) : ", 600);
			YesCh = Integer.parseInt(ReadValue.input());
			if (YesCh == 1) {
				_SetPrint.MSG("안녕! 난 " + getName() + "이야! 너 여기 살아?", 500);
				_SetPrint.MSG("그 친구는 날 무시했다.", 600);
				startpoint = 30;
				endpoint = 60;
				rangepoint = endpoint - startpoint + 1;
				int happyPoint = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
				System.out.println();
				_SetPrint.MSG("기분이 " + happyPoint + "만큼 감소했습니다", 600);
				int uHappy = getHappy();
				uHappy -= happyPoint;
				setHappy(uHappy);
				setLoveStory(999);
			} else {
				setLoveStory(999);
			}
		} else if (LoverCheker == 999) {
			_SetPrint.MSG("요새 항상 보이던 그 사람이 안보인다.", 500);
			_SetPrint.MSG("어디에 간걸까", 500);
			_SetPrint.MSGL("계속돌아 다녀봤지만 별 일 없었다.", 500);
			setLoveStory(1000);
		} else if (LoverCheker == 3) {
			_SetPrint.MSG("어! 그 사람이다!", 600);
			_SetPrint.MSGL("같이 놀자고 할까? (1.예 / 2.아니오) : ", 600);
			YesCh = Integer.parseInt(ReadValue.input());
			if (YesCh == 1) {
				startpoint = 80;
				endpoint = 120;
				rangepoint = endpoint - startpoint + 1;
				int happyPoint = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
				_SetPrint.MSG("즐겁게 놀았다", 600);
				System.out.println();
				_SetPrint.MSG("기분이 " + happyPoint + "만큼 상승했습니다", 600);
				int uHappy = getHappy();
				uHappy += happyPoint;
				setHappy(uHappy);
				setLoveStory(4);
			} else {
				setLoveStory(3);
			}
		} else if (LoverCheker == 4) {
			_SetPrint.MSG("앗! 또 봤다!", 600);
			_SetPrint.MSGL("같이 시내나 돌아 다닐까? (1.예 / 2.아니오) : ", 600);
			YesCh = Integer.parseInt(ReadValue.input());
			if (YesCh == 1) {
				startpoint = 800;
				endpoint = 1300;
				rangepoint = endpoint - startpoint + 1;
				int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
				int happyPoint = randMoney / 10;
				_SetPrint.MSG("앗!! " + randMoney + "원 주웠다!!", 600);
				System.out.println();
				_SetPrint.MSG("돈 " + randMoney + "원을 벌었습니다.", 600);
				_SetPrint.MSG("기분 " + happyPoint + "만큼 상승했습니다.", 600);
				int uMoney = getMoney();
				uMoney += randMoney;
				setMoney(uMoney);
				int uHappy = getHappy();
				uHappy += happyPoint;
				setHappy(uHappy);
				setLoveStory(5);
			} else {
				setLoveStory(4);
			}
		} else {
			_SetPrint.MSG("딱히 아무일도 없었다", 600);
		}
	}

	public void HtmlMaster() {
		ViligeMent();
		startpoint = 150;
		endpoint = 230;
		rangepoint = endpoint - startpoint + 1;
		int randStudy = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		_SetPrint.MSG("HTML마스터를 만났다!", 600);
		_SetPrint.MSG("그분께 조언을 얻었다!", 600);
		_SetPrint.MSG("HTML 능력치가 " + randStudy + "만큼 상승했습니다", 600);
		int uStudyLevel = getHtml();
		uStudyLevel += randStudy;
		setHtml(uStudyLevel);
	}
	
	public void JqueryMaster() {
		ViligeMent();
		startpoint = 150;
		endpoint = 230;
		rangepoint = endpoint - startpoint + 1;
		int randStudy = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		_SetPrint.MSG("JavaScript/Jquery마스터를 만났다!", 600);
		
		if (getHtml() > 200) {
			_SetPrint.MSG("그분께 조언을 얻었다!", 600);
			_SetPrint.MSG("JavaScript/Jquery 능력치가 " + randStudy + "만큼 상승했습니다", 600);
			int uStudyLevel = getJquery();
			uStudyLevel += randStudy;
			setJquery(uStudyLevel);
		} else {
			_SetPrint.MSG("그러나, 내 HTML실력이 부족해서 마스터는 HTML을 조금 알려줬다", 600);
			startpoint = 50;
			endpoint = 70;
			rangepoint = endpoint - startpoint + 1;
			randStudy = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
			int uStudyLevel = getHtml();
			uStudyLevel += randStudy;
			setHtml(uStudyLevel);
			_SetPrint.MSG("HTML 능력치가 " + randStudy + "만큼 상승했습니다", 600);
		}
	}
	
	public void JavaMaster() {
		ViligeMent();
		startpoint = 150;
		endpoint = 230;
		rangepoint = endpoint - startpoint + 1;
		int randStudy = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		_SetPrint.MSG("Java 마스터를 만났다!", 600);
		_SetPrint.MSG("그분께 조언을 얻었다!", 600);
		_SetPrint.MSG("Java 능력치가 " + randStudy + "만큼 상승했습니다", 600);
		int uStudyLevel = getJava();
		uStudyLevel += randStudy;
		setJava(uStudyLevel);
	}
	
	public void BestAlba() {
		int max=0;
		String maxName="";
		
		for(int i=0;i<getdata.length;i++){
            // 최대값 Max
            if(getdata[i] > max){
                  max = getdata[i];
                  maxName=dataname[i];
            }
		}
		if (max>150) {
			_SetPrint.MSG("너 요새 " + maxName + "좀 잘하는거 같은데!", 600);
			_SetPrint.MSG(maxName + "알바를 하면 돈을 많이 벌 수 있을꺼야!", 600);
			_SetPrint.MSG(getName() + "!! 거부권은 없어!", 600);
			
			startpoint = 900;
			endpoint = 1500;
			rangepoint = endpoint - startpoint + 1;
			int randMoney = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
			int happyPoint = randMoney / 30;
			System.out.println();
			_SetPrint.MSG("열심히 일했다", 800);
			_SetPrint.MSG("돈 " + randMoney + "원을 벌었습니다.", 600);
			_SetPrint.MSG("기분 " + happyPoint + "만큼 상승했습니다.", 600);
			int uMoney = getMoney();
			uMoney += randMoney;
			setMoney(uMoney);
			int uHappy = getHappy();
			uHappy += happyPoint;
			setHappy(uHappy);
		} else {
			_SetPrint.MSG("딱히 아무일도 일어나지 않았다", 600);
		}
		
	}
	
	
	
}

