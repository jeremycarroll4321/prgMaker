﻿package _NewGame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.SystemColor;

public class _GUIApp extends YourData {
	YourData yd = new YourData();
	YourMaker ym = new YourMaker();
	ItemSaver isv = new ItemSaver();
	private JFrame frame;
	_Ment ment = new _Ment();
	int opCheck = 0,tutoMent=0,firstMeet=0,todayHello = 0,buyNumber=0;
	int timerCheck=0,noMoneyCheck=0;

	JDialog dia1_name, dial2, dial3,dial_today,dia1_myInfo,storeFirstBox,storeBuybox,storeSellbox,sellOkBox;

	JLabel lblMoriningNotice,lblStudyNotice;
	private JTextField SellNumTxt;
	private JTextField textName,textAge,textJender;
	JLabel labelHappy = new JLabel();
	JLabel lblNewLabel = new JLabel("");
	JLabel labelDayPoint  = new JLabel("");
	JPanel RightOption = new JPanel();
	JTextArea txtAr = new JTextArea();
	JTextArea txtrFf;
	JLabel lbl_MyName = new JLabel();
	JLabel labelMoney = new JLabel();
	JLabel labelDay = new JLabel();
	JTextArea StudyTextArea;
	JPanel StudyGoGo;
	JPanel LeftOption;

	
	
	private JButton btnWorkHtml,btnWorkJquery,btnWorkPhp,btnWorkAsp,btnWorkJsp,btnWorkClang,btnWorkCpp,btnWorkCshop,btnWorkJava,btnWorkPython,btnWorkObjective,btnWorkSwift, btnPlayAlba,btnPlayVacation,btnPlayGoOut;
	private JButton itemBuy_Lotto,itemBuy_DaeChul,itemBuy_Book,itemBuy_OldLaptop,itemBuy_NewLaptop,itemBuy_Apple,itemBuy_airobot,itemBuy_loveFriend,itemBuy_RealBtn,itemBuy_helper;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					_GUIApp window = new _GUIApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public _GUIApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	public void MoneySet() { //자금설정
		labelMoney.setText("자금 : " + getMoney() + "원");
	}
	public void DaySet() { //날짜설정
		labelDay.setText("" + yd.getThisDay());
	}	
	public void HappySet() { //기분설정
		labelHappy.setText("기분 : " + getHappy());
	}		
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);
		
		LeftOption = new JPanel();
		
		LeftOption.setBackground(new Color(130, 49, 32, 0));
		//LeftOption.setBounds(12, 10, 201, 270);//왼쪽창 활성화
		LeftOption.setBounds(0, 0, 0, 0);
		frame.getContentPane().add(LeftOption);
		LeftOption.setLayout(null);
		labelHappy.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		labelHappy.setForeground(new Color(128, 0, 0));
		labelHappy.setFont(new Font("맑은 고딕", Font.BOLD, 14));
		labelHappy.setBounds(22, 210, 159, 25);
		LeftOption.add(labelHappy);
		
		JLabel labelDayText = new JLabel();

		labelDayText.setBounds(166, 110, 28, 20);
		LeftOption.add(labelDayText);
		labelDayText.setText("Day");
		labelDayText.setForeground(new Color(0, 0, 0));
		labelDayText.setFont(new Font("맑은 고딕", Font.BOLD, 14));
		labelDay.setText(" ");
		labelDay.setHorizontalAlignment(SwingConstants.RIGHT);
		
		
		labelDay.setForeground(Color.RED);
		labelDay.setFont(new Font("맑은 고딕", Font.BOLD, 23));
		labelDay.setBounds(122, 125, 72, 30);
		LeftOption.add(labelDay);
		lblNewLabel.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/men_basic.png")));
		
		
		
		lblNewLabel.setBounds(12, 20, 169, 135);
		LeftOption.add(lblNewLabel);
		lbl_MyName.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_MyName.setText("생성준비중");
		lbl_MyName.setForeground(new Color(160, 82, 45));
		
		
		lbl_MyName.setFont(new Font("맑은 고딕", Font.BOLD, 17));
		lbl_MyName.setBounds(12, 155, 182, 42);
		LeftOption.add(lbl_MyName);
		labelMoney.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		labelMoney.setForeground(new Color(172, 36, 94));
		labelMoney.setFont(new Font("맑은 고딕", Font.BOLD, 14));
		labelMoney.setBounds(22, 190, 159, 25);
		LeftOption.add(labelMoney);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(240, 182, 119, 0));
		panel_3.setBounds(12, 155, 182, 115);
		LeftOption.add(panel_3);
		
		JLabel lbl_LeftOpt_bg = new JLabel("");
		lbl_LeftOpt_bg.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/profileTools.png")));
		lbl_LeftOpt_bg.setBounds(0, 0, 201, 270);
		LeftOption.add(lbl_LeftOpt_bg);
		
		//JPanel RightOption = new JPanel();
		RightOption = new JPanel();
		frame.getContentPane().add(RightOption);
		RightOption.setBackground(new Color(105, 105, 105));
		//RightOption.setBounds(659, 0, 135, 298);
		RightOption.setBounds(0, 0, 0, 0);

		RightOption.setLayout(null);
		
	
		//팝업창
		JPanel PopupLayer = new JPanel();
		PopupLayer.setBounds(0, 0, 0, 0);
		PopupLayer.setPreferredSize(new Dimension(800,600));
		PopupLayer.setBackground(new Color(0, 0, 0, 50));
		frame.getContentPane().add(PopupLayer);
		PopupLayer.setLayout(null);
		
		//아침
		 /*
		lblMoriningNotice = new JLabel(" ");
		lblMoriningNotice.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/goodmorining.png")));
		//lblMoriningNotice.setBounds(228, 175, 475, 280);
		lblMoriningNotice.setBounds(0, 0, 0, 0);
		PopupLayer.add(lblMoriningNotice);
		
		//공부
		lblStudyNotice = new JLabel(" ");
		lblStudyNotice.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/studyPrograming.png")));
		//lblStudyNotice.setBounds(228, 175, 475, 280);
		lblStudyNotice.setBounds(0, 0, 0, 0);
		PopupLayer.add(lblStudyNotice);
	*/
	
		
		JButton btnStudy = new JButton("");
		btnStudy.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/sceIcon.png")));
		btnStudy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DaySetting(); // 일정관리
				dial_today.pack();
				
			}
		});
		btnStudy.setFocusable(false);
		btnStudy.setContentAreaFilled(false);
		btnStudy.setBorderPainted(false);
		btnStudy.setBounds(16, 10, 107, 86);
		RightOption.add(btnStudy);
		
		
		
		JButton btnIfno = new JButton("");
		btnIfno.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/sceIcon2.png")));
		btnIfno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				myInfoView(); //내정보
				dia1_myInfo.pack();
				
			}
		});
		btnIfno.setFocusable(false);
		btnIfno.setContentAreaFilled(false);
		btnIfno.setBorderPainted(false);
		btnIfno.setBounds(16, 102, 107, 87);
		RightOption.add(btnIfno);
		
		JButton btnItemStore = new JButton("");
		btnItemStore.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/sceIcon3.png")));
		btnItemStore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StoreFirst(); //내정보
				storeFirstBox.pack();
				
			}
		});
		btnItemStore.setFocusable(false);
		btnItemStore.setContentAreaFilled(false);
		btnItemStore.setBorderPainted(false);
		btnItemStore.setBounds(16, 191, 107, 97);
		RightOption.add(btnItemStore);
		
		
		
		
		JPanel BottomOption = new JPanel();
		BottomOption.setBackground(new Color(55, 71, 79));
		BottomOption.setBounds(0, 444, 794, 128);
		frame.getContentPane().add(BottomOption);
		BottomOption.setLayout(null);
		txtAr.setForeground(new Color(223, 231, 235));
		txtAr.setBackground(new Color(55, 71, 79));
		txtAr.setEditable(false);
		txtAr.setLineWrap(true);
		
		
		txtAr.setFont(new Font("맑은 고딕", Font.BOLD, 18));
		txtAr.setText("다음 버튼을 눌러 게임을 시작하십시오");
		txtAr.setBounds(12, 21, 648, 98);
		BottomOption.add(txtAr);
		

		JButton btnNext = new JButton("");
		btnNext.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/nextBtn.png")));
		btnNext.setFocusable(false);
		btnNext.setContentAreaFilled(false);
		btnNext.setBorderPainted(false);
		DayByebye dbye = new DayByebye();
		btnNext.addActionListener(dbye);
		//StudyCoading stco = new StudyCoading();
		//btnNext.addActionListener(stco);
		
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (opCheck <= 3) {
				txtAr.setText(ment.opMentWindow(opCheck++));
				} else if (opCheck == 4){
					LeftOption.setBounds(12, 10, 201, 270);//왼쪽창 활성화
					nameSetting(); //이름입력창 활성화
					dia1_name.pack();
				}
				if (opCheck == 5) {
					if (tutoMent <= 6) {
						txtAr.setText(ment.FristTutorialMsgWindow(tutoMent++));
					} else if (tutoMent == 7 && firstMeet == 0) {
						txtAr.setText("반가워!");
						
						
						alwaysUpload(); //항상 바뀌는것
						setWorkTime(4);
						LelvelInfoWindow(); // 레벨창 활성화
						firstMeet = 1;
						todayHello=1;
						
						if (todayHello==1) {
							RightOption.setBounds(659, 0, 135, 298); // 오른쪽창 활성화
							txtAr.setText(ment.helloMessage());
							todayHello=1;
						} else if (todayHello==2) {
							
						}
							
					
						
						
					}
				}
					
			}
		});
		btnNext.setBounds(715, 21, 79, 97);
		BottomOption.add(btnNext);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/Lookme.png")));
		lblNewLabel_1.setBounds(0, 0, 794, 135);
		BottomOption.add(lblNewLabel_1);
		
		JLabel MainBg = new JLabel("");
		MainBg.setBackground(SystemColor.activeCaptionText);
		MainBg.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/Morning.jpg")));
		MainBg.setBounds(0, 0, 794, 572);
		frame.getContentPane().add(MainBg);
	}
	
	
	
	class DayByebye implements ActionListener { //하루가 지나면~
		public void actionPerformed(ActionEvent e4) {
			if (todayHello==1) {
				RightOption.setBounds(659, 0, 135, 298); // 오른쪽창 활성화
				txtAr.setText(ment.helloMessage());
				todayHello=5;
				setWorkTime(4);
			}
			
		}
	}	
	
	
	
	
	
	public void mentPrint(String txt) {
		txtAr.setText(txt);
	}
	
	public void alwaysUpload() {
		MoneySet(); //돈설정
		DaySet(); //날짜설정
		HappySet(); //기분설정
	}
	
	public void alwaysCheck() {
		if (yd.getWorkTime() <= 0) {
			//txtAr.setText("오늘 하루 끝!");
			
			alwaysUpload(); //항상 바뀌는것
			RightOption.setBounds(0, 0, 0, 0);
			txtAr.setText(ment.byeMessage());
			
			todayHello=1;
			int dayIfo = getThisDay();
			dayIfo+=1;
			setThisDay(dayIfo);
			setWorkTime(4);
		}
	}
	
	public void LRMenuHide() {
		LeftOption.setBounds(0,0,0,0);//왼쪽창 활성화
		RightOption.setBounds(0, 0, 0, 0);
	}
	public void LRMenuShow() {
		LeftOption.setBounds(12, 10, 201, 270);//왼쪽창 활성화
		RightOption.setBounds(659, 0, 135, 298); // 오른쪽창 활성화
	}
	
	public void sellOkBoxForm() {
		
		sellOkBox=new JDialog();
		sellOkBox.setLocation(500, 200);
		sellOkBox.setResizable(false);
		sellOkBox.setSize(246, 154);
		sellOkBox.setVisible(true);
		
		JPanel panelSellOk = new JPanel();
		panelSellOk.setBounds(0, 0, 246, 154);
		sellOkBox.getContentPane().add(panelSellOk);
		panelSellOk.setLayout(null);
		panelSellOk.setPreferredSize(new Dimension(246,154));
		
		SellNumTxt = new JTextField();
		SellNumTxt.setFont(new Font("굴림", Font.PLAIN, 15));
		SellNumTxt.setBounds(35, 62, 177, 30);
		panelSellOk.add(SellNumTxt);
		SellNumTxt.setColumns(10);
		
		JButton btnSellOk = new JButton("");
		btnSellOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int sellNums = Integer.parseInt(SellNumTxt.getText());
				isv.itemDel(sellNums);
				ReLoadSellList();
				MoneySet();
				sellOkBox.dispose();
				
			}
		});
		btnSellOk.setFocusable(false);
		btnSellOk.setContentAreaFilled(false);
		btnSellOk.setBorderPainted(false);
		btnSellOk.setBounds(89, 102, 66, 42);
		panelSellOk.add(btnSellOk);
		
		JLabel storeBackground = new JLabel("");
		storeBackground.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/inputSellNumber.jpg")));
		storeBackground.setBounds(0, 0, 246, 154);
		panelSellOk.add(storeBackground);
	}
	
	public void StoreSell() {
		
		storeSellbox=new JDialog();
		storeSellbox.setLocation(500, 200);
		storeSellbox.setResizable(false);
		storeSellbox.setSize(265, 250);
		storeSellbox.setVisible(true);
		
		
		JPanel panelSell = new JPanel();
		panelSell.setBounds(0, 0, 500, 450);
		storeSellbox.getContentPane().add(panelSell);
		panelSell.setLayout(null);
		panelSell.setPreferredSize(new Dimension(500,450));
		
		JButton itemSell_RealBtn = new JButton("");
		itemSell_RealBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				sellOkBoxForm();
				sellOkBox.pack();
				
			}
		});
		itemSell_RealBtn.setFocusable(false);
		itemSell_RealBtn.setContentAreaFilled(false);
		itemSell_RealBtn.setBorderPainted(false);
		itemSell_RealBtn.setBounds(327, 39, 161, 74);
		panelSell.add(itemSell_RealBtn);
		
		txtrFf = new JTextArea();
		txtrFf.setFont(new Font("맑은 고딕", Font.PLAIN, 16));
		txtrFf.setText("");
		txtrFf.append(isv.itemLoad_Win());
		txtrFf.setEditable(false);
		txtrFf.setForeground(Color.WHITE);
		txtrFf.setBackground(Color.DARK_GRAY);
		txtrFf.setBounds(28, 53, 268, 367);
		panelSell.add(txtrFf);
		
		JLabel storeBackground = new JLabel("");
		storeBackground.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/SellSystem.jpg")));
		storeBackground.setBounds(0, 0, 500, 450);
		panelSell.add(storeBackground);
	}
	
	public void StoreBuy() {
		buyNumber=0;
		storeBuybox=new JDialog();
		storeBuybox.setLocation(500, 200);
		storeBuybox.setResizable(false);
		storeBuybox.setSize(265, 250);
		storeBuybox.setVisible(true);
		
		JPanel buyPanel = new JPanel();
		buyPanel.setBounds(0, 0, 500, 450);
		storeBuybox.getContentPane().add(buyPanel);
		buyPanel.setLayout(null);
		
		itemBuy_Lotto = new JButton("");
		itemBuy_Lotto.setBounds(23, 53, 282, 36);
		//itemBuy_Lotto.setFocusable(false);
		//itemBuy_Lotto.setFocu;
		itemBuy_Lotto.setContentAreaFilled(false);
		itemBuy_Lotto.setBorderPainted(false);
		buyPanel.setPreferredSize(new Dimension(500,450));
		buyPanel.add(itemBuy_Lotto);

		
		itemBuy_DaeChul = new JButton("");
		//itemBuy_DaeChul.setFocusable(false);
		itemBuy_DaeChul.setContentAreaFilled(false);
		itemBuy_DaeChul.setBorderPainted(false);
		itemBuy_DaeChul.setBounds(23, 92, 282, 36);
		buyPanel.add(itemBuy_DaeChul);
		
		itemBuy_Book = new JButton("");
		//itemBuy_Book.setFocusable(false);
		itemBuy_Book.setContentAreaFilled(false);
		itemBuy_Book.setBorderPainted(false);
		itemBuy_Book.setBounds(23, 130, 282, 36);
		buyPanel.add(itemBuy_Book);
		
		itemBuy_OldLaptop = new JButton("");
		//itemBuy_OldLaptop.setFocusable(false);
		itemBuy_OldLaptop.setContentAreaFilled(false);
		itemBuy_OldLaptop.setBorderPainted(false);
		itemBuy_OldLaptop.setBounds(23, 173, 282, 36);
		buyPanel.add(itemBuy_OldLaptop);
		
		itemBuy_NewLaptop = new JButton("");
		//itemBuy_NewLaptop.setFocusable(false);
		itemBuy_NewLaptop.setContentAreaFilled(false);
		itemBuy_NewLaptop.setBorderPainted(false);
		itemBuy_NewLaptop.setBounds(23, 213, 282, 36);
		buyPanel.add(itemBuy_NewLaptop);
		
		itemBuy_Apple = new JButton("");
		//itemBuy_Apple.setFocusable(false);
		itemBuy_Apple.setContentAreaFilled(false);
		itemBuy_Apple.setBorderPainted(false);
		itemBuy_Apple.setBounds(23, 258, 282, 36);
		buyPanel.add(itemBuy_Apple);
		
		itemBuy_helper = new JButton("");
		//itemBuy_helper.setFocusable(false);
		itemBuy_helper.setContentAreaFilled(false);
		itemBuy_helper.setBorderPainted(false);
		itemBuy_helper.setBounds(23, 296, 282, 36);
		buyPanel.add(itemBuy_helper);
		

		itemBuy_airobot = new JButton("");
		//itemBuy_airobot.setFocusable(false);
		itemBuy_airobot.setContentAreaFilled(false);
		itemBuy_airobot.setBorderPainted(false);
		itemBuy_airobot.setBounds(23, 339, 282, 36);
		buyPanel.add(itemBuy_airobot);
		
		itemBuy_loveFriend = new JButton("");
		//itemBuy_loveFriend.setFocusable(false);
		itemBuy_loveFriend.setContentAreaFilled(false);
		itemBuy_loveFriend.setBorderPainted(false);
		itemBuy_loveFriend.setBounds(23, 382, 282, 36);
		buyPanel.add(itemBuy_loveFriend);
		
		itemBuy_RealBtn = new JButton("");
		//itemBuy_RealBtn.setFocusable(false);
		itemBuy_RealBtn.setContentAreaFilled(false);
		itemBuy_RealBtn.setBorderPainted(false);
		itemBuy_RealBtn.setBounds(327, 39, 161, 74);
		buyPanel.add(itemBuy_RealBtn);
		
		JLabel storeBackground = new JLabel("");
		storeBackground.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/buySystem.jpg")));
		storeBackground.setBounds(0, 0, 500, 450);
		buyPanel.add(storeBackground);
		
		MyBuySystem myBuy = new MyBuySystem();
		itemBuy_Lotto.addActionListener(myBuy);
		itemBuy_DaeChul.addActionListener(myBuy);
		itemBuy_Book.addActionListener(myBuy);
		itemBuy_OldLaptop.addActionListener(myBuy);
		itemBuy_NewLaptop.addActionListener(myBuy);
		itemBuy_Apple.addActionListener(myBuy);
		itemBuy_helper.addActionListener(myBuy);
		itemBuy_airobot.addActionListener(myBuy);
		itemBuy_loveFriend.addActionListener(myBuy);
		itemBuy_RealBtn.addActionListener(myBuy);
		
	}
	public void BuyOk(String Name) {
		JOptionPane.showMessageDialog(null, Name + "을 구매하였습니다");
	}
	class MyBuySystem implements ActionListener{ //구매시슽ㅁ체크
		
		public void actionPerformed(ActionEvent e6) {
			 Object o=e6.getSource();
			if(o==itemBuy_Lotto)
				buyNumber=1;
			else if(o==itemBuy_DaeChul)
				buyNumber=2;
			else if(o==itemBuy_Book)
				buyNumber=3;
			else if(o==itemBuy_OldLaptop)
				buyNumber=4;
			else if(o==itemBuy_NewLaptop)
				buyNumber=5;
			else if(o==itemBuy_Apple)
				buyNumber=6;
			else if(o==itemBuy_helper)
				buyNumber=7;
			else if(o==itemBuy_airobot)
				buyNumber=8;
			else if(o==itemBuy_loveFriend)
				buyNumber=9;
			else {}
			
			if(o==itemBuy_RealBtn) {
				ItemInfo itemFo = new ItemInfo();
				Lotto lts = new Lotto();
				//ItemSaver isv = new ItemSaver();
					if(buyNumber==1) {
						if (getMoney() >= 100) {
							lts.ev_start();
						} else {
							noMoney();
						}
					} else if(buyNumber==2) {
						ItemStore ist = new ItemStore();
						ist.realDaechul();
					} else if(buyNumber==3) {
						if (getMoney() >= 900) {
							itemFo.i_books();
							isv.itemSave(itemFo);
							BuyOk("전문서적");
						} else {
							noMoney();
						}
					} else if(buyNumber==4) {
						if (getMoney() >= 1400) {
							itemFo.i_note(); 
							isv.itemSave(itemFo);
							BuyOk("보급 노트북");
						} else {
							noMoney();
						}
					} else if(buyNumber==5) {
						if (getMoney() >= 2000) {
							itemFo.i_note2(); 
							isv.itemSave(itemFo);
							BuyOk("최신 노트북");
						} else {
							noMoney();
						}
					} else if(buyNumber==6) {
						if (getMoney() >= 3000) {
							itemFo.i_apple(); 
							isv.itemSave(itemFo);
							BuyOk("Apple기기");
						} else {
						}
					} else if(buyNumber==7) {
						if (getMoney() >= 2300) {
							itemFo.i_helper(); 
							isv.itemSave(itemFo);
							BuyOk("도우미");
						} else {
						}
					} else if(buyNumber==8) {
						if (getMoney() >= 8500) {
							itemFo.i_aihelper(); 
							isv.itemSave(itemFo);
							BuyOk("AI로봇");
						} else {
						}
					} else if(buyNumber==9) {
						if (getMoney() >= -9999) {
							itemFo.i_friends(); 
							isv.itemSave(itemFo);
							BuyOk("애인");
						} else {
						}
					} else if(buyNumber==0) {
						JOptionPane.showMessageDialog(null, "물건을 선택해주세요.");
					}
					MoneySet();
					buyNumber=0;
			} 
		}
	}
	
	public void noMoney() {
		JOptionPane.showMessageDialog(null, "잔액이 부족합니다.");
	}
	public void StoreFirst() {
		
		storeFirstBox=new JDialog();
		storeFirstBox.setLocation(500, 200);
		storeFirstBox.setResizable(false);
		storeFirstBox.setSize(265, 250);
		storeFirstBox.setVisible(true);
		
		JPanel panelBoxS = new JPanel();
		panelBoxS.setBounds(0, 0, 265, 250);
		panelBoxS.setPreferredSize(new Dimension(265,250));
		storeFirstBox.getContentPane().add(panelBoxS);
		panelBoxS.setLayout(null);
		
		JButton btnStoreBuy = new JButton("");
		btnStoreBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StoreBuy();
				storeBuybox.pack();
			}
		});
		btnStoreBuy.setBounds(12, 10, 240, 113);
		btnStoreBuy.setFocusable(false);
		btnStoreBuy.setContentAreaFilled(false);
		btnStoreBuy.setBorderPainted(false);
		panelBoxS.add(btnStoreBuy);
		
		JButton btnStoreSell = new JButton("");
		btnStoreSell.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StoreSell();
				ReLoadSellList();
				storeSellbox.pack();
				
			}
		});
		btnStoreSell.setBounds(12, 133, 241, 107);
		btnStoreSell.setFocusable(false);
		btnStoreSell.setContentAreaFilled(false);
		btnStoreSell.setBorderPainted(false);
		panelBoxS.add(btnStoreSell);
		
		JLabel storeBackground = new JLabel("");
		storeBackground.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/Storebg.jpg")));
		storeBackground.setBounds(0, 0, 265, 250);
		panelBoxS.add(storeBackground);
	}
	
	public void DaySetting() {
		dial_today=new JDialog();
		dial_today.setLocation(500, 200);
		dial_today.setResizable(false);
		dial_today.setSize(461, 519);
		dial_today.setVisible(true);
	
		JPanel panel75 = new JPanel();
		panel75.setBounds(0, 0, 461, 519);
		dial_today.getContentPane().add(panel75);
		panel75.setLayout(null);
		panel75.setPreferredSize(new Dimension(460,520));


		
		btnWorkHtml = new JButton("");
		
		btnWorkHtml.setToolTipText("Html/CSS를 배웁니다. 요금 : " + getHtmlMoney() + "원");
		btnWorkHtml.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnHtml.png")));
		btnWorkHtml.setBounds(12, 34, 131, 63);
		btnWorkHtml.setBorderPainted(false);
		btnWorkHtml.setContentAreaFilled(false);
		btnWorkHtml.setFocusable(false);
		panel75.add(btnWorkHtml);
		
		btnWorkJquery = new JButton("");
		btnWorkJquery.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnJquery.png")));
		btnWorkJquery.setToolTipText("JavaScript/Jquery를 배웁니다. 요금 : " + getJqueryMoney() + "원");
		btnWorkJquery.setFocusable(false);
		btnWorkJquery.setContentAreaFilled(false);
		btnWorkJquery.setBorderPainted(false);
		btnWorkJquery.setBounds(144, 34, 131, 63);
		panel75.add(btnWorkJquery);
		
		btnWorkPhp = new JButton("");
		btnWorkPhp.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnPhp.png")));
		btnWorkPhp.setToolTipText("php를 배웁니다. 요금 : " + getPhpMoney() + "원");
		btnWorkPhp.setFocusable(false);
		btnWorkPhp.setContentAreaFilled(false);
		btnWorkPhp.setBorderPainted(false);
		btnWorkPhp.setBounds(12, 107, 131, 63);
		panel75.add(btnWorkPhp);
		
		btnWorkAsp = new JButton("");
		btnWorkAsp.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnAsp.png")));
		btnWorkAsp.setToolTipText("asp를 배웁니다. 요금 : " + getAspMoney() + "원");
		btnWorkAsp.setFocusable(false);
		btnWorkAsp.setContentAreaFilled(false);
		btnWorkAsp.setBorderPainted(false);
		btnWorkAsp.setBounds(144, 107, 131, 63);
		panel75.add(btnWorkAsp);
		
		btnWorkJsp = new JButton("");
		btnWorkJsp.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnJsp.png")));
		btnWorkJsp.setToolTipText("jsp를 배웁니다. 요금 : " + getJspMoney() + "원");
		btnWorkJsp.setFocusable(false);
		btnWorkJsp.setContentAreaFilled(false);
		btnWorkJsp.setBorderPainted(false);
		btnWorkJsp.setBounds(277, 107, 131, 63);
		panel75.add(btnWorkJsp);
		
		btnWorkClang = new JButton("");
		btnWorkClang.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnC.png")));
		btnWorkClang.setToolTipText("c언어를 배웁니다. 요금 : " + getClangMoney() + "원");
		btnWorkClang.setFocusable(false);
		btnWorkClang.setContentAreaFilled(false);
		btnWorkClang.setBorderPainted(false);
		btnWorkClang.setBounds(12, 180, 131, 63);
		panel75.add(btnWorkClang);
		
		btnWorkCpp = new JButton("");
		btnWorkCpp.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnCpp.png")));
		btnWorkCpp.setToolTipText("c++를 배웁니다. 요금 : " + getCppMoney() + "원");
		btnWorkCpp.setFocusable(false);
		btnWorkCpp.setContentAreaFilled(false);
		btnWorkCpp.setBorderPainted(false);
		btnWorkCpp.setBounds(144, 180, 131, 63);
		panel75.add(btnWorkCpp);
		
		btnWorkCshop = new JButton("");
		btnWorkCshop.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnCshop.png")));
		btnWorkCshop.setToolTipText("c#을 배웁니다. 요금 : " + getCshopMoney() + "원");
		btnWorkCshop.setFocusable(false);
		btnWorkCshop.setContentAreaFilled(false);
		btnWorkCshop.setBorderPainted(false);
		btnWorkCshop.setBounds(277, 180, 131, 63);
		panel75.add(btnWorkCshop);
		
		btnWorkJava = new JButton("");
		btnWorkJava.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnJava.png")));
		btnWorkJava.setToolTipText("Java를 배웁니다. 요금 : " + getJavaMoney() + "원");
		btnWorkJava.setFocusable(false);
		btnWorkJava.setContentAreaFilled(false);
		btnWorkJava.setBorderPainted(false);
		btnWorkJava.setBounds(144, 253, 131, 63);
		panel75.add(btnWorkJava);
		
		btnWorkPython = new JButton("");
		btnWorkPython.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnPython.png")));
		btnWorkPython.setToolTipText("Python를 배웁니다. 요금 : " + getPythonMoney() + "원");
		btnWorkPython.setFocusable(false);
		btnWorkPython.setContentAreaFilled(false);
		btnWorkPython.setBorderPainted(false);
		btnWorkPython.setBounds(12, 253, 131, 63);
		panel75.add(btnWorkPython);
		
		btnWorkObjective = new JButton("");
		btnWorkObjective.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnObjective.png")));
		btnWorkObjective.setToolTipText("Objective-C를 배웁니다. 요금 : " + getObject_cMoney() + "원");
		btnWorkObjective.setFocusable(false);
		btnWorkObjective.setContentAreaFilled(false);
		btnWorkObjective.setBorderPainted(false);
		btnWorkObjective.setBounds(12, 326, 131, 63);
		panel75.add(btnWorkObjective);
		
		btnWorkSwift = new JButton("");
		btnWorkSwift.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnSwift.png")));
		btnWorkSwift.setToolTipText("Swift를 배웁니다 요금 : " + getSwiftMoney() + "원");
		btnWorkSwift.setFocusable(false);
		btnWorkSwift.setContentAreaFilled(false);
		btnWorkSwift.setBorderPainted(false);
		btnWorkSwift.setBounds(144, 326, 131, 63);
		panel75.add(btnWorkSwift);
		
		JLabel labelStudyTitle = new JLabel("공부일정");
		labelStudyTitle.setBounds(24, 10, 57, 15);
		panel75.add(labelStudyTitle);
		
		JLabel labelEtcTitle = new JLabel("휴식/기타일정");
		labelEtcTitle.setBounds(12, 409, 92, 15);
		panel75.add(labelEtcTitle);
		
		btnPlayAlba = new JButton("");
		btnPlayAlba.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnAlba.png")));
		btnPlayAlba.setToolTipText("아르바이트를 합니다.");
		btnPlayAlba.setFocusable(false);
		btnPlayAlba.setContentAreaFilled(false);
		btnPlayAlba.setBorderPainted(false);
		btnPlayAlba.setBounds(12, 430, 131, 63);
		panel75.add(btnPlayAlba);
		
		btnPlayVacation = new JButton("");
		btnPlayVacation.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnVacation.png")));
		btnPlayVacation.setToolTipText("휴식을 취합니다");
		btnPlayVacation.setFocusable(false);
		btnPlayVacation.setContentAreaFilled(false);
		btnPlayVacation.setBorderPainted(false);
		btnPlayVacation.setBounds(144, 430, 131, 63);
		panel75.add(btnPlayVacation);
		
		btnPlayGoOut = new JButton("");
		btnPlayGoOut.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/StudyBtnGoout.png")));
		btnPlayGoOut.setToolTipText("외출합니다.");
		btnPlayGoOut.setFocusable(false);
		btnPlayGoOut.setContentAreaFilled(false);
		btnPlayGoOut.setBorderPainted(false);
		btnPlayGoOut.setBounds(277, 430, 131, 63);
		panel75.add(btnPlayGoOut);
		
		JLabel labelDayPoint = new JLabel("");
		labelDayPoint.setText("" + yd.getWorkTime());
		labelDayPoint.setForeground(new Color(128, 0, 0));
		labelDayPoint.setHorizontalAlignment(SwingConstants.RIGHT);
		labelDayPoint.setFont(new Font("Comic Sans MS", Font.BOLD, 28));
		labelDayPoint.setBounds(355, 15, 32, 57);
		panel75.add(labelDayPoint);
		
		JLabel lblDaypointimg = new JLabel("DayPointImg");
		lblDaypointimg.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/dayPoint.png")));
		lblDaypointimg.setBounds(390, 13, 59, 63);
		panel75.add(lblDaypointimg);

		
		MyHandler my = new MyHandler();
		btnWorkHtml.addActionListener(my);
		btnWorkJquery.addActionListener(my);
		btnWorkPhp.addActionListener(my);
		btnWorkAsp.addActionListener(my);
		btnWorkJsp.addActionListener(my);
		btnWorkClang.addActionListener(my);
		btnWorkCpp.addActionListener(my);
		btnWorkCshop.addActionListener(my);
		btnWorkJava.addActionListener(my);
		btnWorkPython.addActionListener(my);
		btnWorkObjective.addActionListener(my);
		btnWorkSwift.addActionListener(my);
		btnPlayAlba.addActionListener(my);
		btnPlayVacation.addActionListener(my);
		btnPlayGoOut.addActionListener(my);
		
	}
	/*
	public void consolView() {
		//txtAr.setText("");
	}*/
	


	class MyHandler implements ActionListener{
		StudyLevel sl = new StudyLevel();
		String subjectName = "";
		 public void actionPerformed(ActionEvent e4) {
			// alwaysCheck(); //상시체크
			 LRMenuHide();
			 dial_today.dispose();
			 
			 
			 Object o=e4.getSource();
			 
			 if(o==btnWorkHtml) {
				 sl.html();
				 
				 
			 } else if(o==btnWorkJquery) {
				 sl.jquery();
					 
				 
				 
				 
			 }else if(o==btnWorkPhp) {
				 sl.php();
					 
				 
				 
			 }else if(o==btnWorkAsp) {
				 sl.asp();
					 
				 
				 
			 }else if(o==btnWorkJsp) {
				 sl.jsp();
					 
					 
				 
				 
			 }else if(o==btnWorkClang) {
				 sl.clnag();
			 }else if(o==btnWorkCpp) {
				 sl.cpp();
			 }else if(o==btnWorkCshop) {
				 sl.cshop();
			 }else if(o==btnWorkJava) {
				 sl.java();
			 }else if(o==btnWorkPython) {
				 sl.python();
			 }else if(o==btnWorkObjective) {
				 sl.objective();
			 }else if(o==btnWorkSwift) {
				 sl.swift();
			 }else if(o==btnPlayAlba) {
				 sl.alba();
			 }else if(o==btnPlayVacation) {
				 sl.vacation();
			 }else if(o==btnPlayGoOut) {
				 sl.goOut();
				 //LevelInfoPrint(subjectName);
			 }
			 
			 
			 alwaysUpload(); //항상 바뀌는것
			 alwaysCheck(); //상시체크
			 LRMenuShow(); //최종보이기

		 }
	 }
	/*
	public void printMoriningLogo() {
		lblMoriningNotice.setBounds(228, 175, 475, 280);
	}
	public void hideMoriningLogo() {
		lblMoriningNotice.setBounds(0, 0, 0, 0);
	}
	
	public void printStudyLogo() {
		lblStudyNotice.setBounds(228, 175, 475, 280);
	}
	public void hideStudyLogo() {
		lblStudyNotice.setBounds(0, 0, 0, 0);
	}
	
	public void noMoneyPrint(String subject) {
		txtAr.setText("앗..! 정말 배우고싶은 " + subject + "...!");
		noMoneyCheck = 100;
	}
	
	public void noStudysPrint(String subject) {
		txtAr.setText("앗..! 정말 배우고싶은 " + subject + "...!");
		noMoneyCheck = 200;
	}
	
	public void LevelInfoPrint(String subject) {
		LRMenuHide();
		printStudyLogo();
		txtAr.setText(subject + "를 해야지!");
		txtAr.setText("열심히 배우는중...");
		timerCheck=300;
		//Timer timer = new Timer();
		//timer.schedule(new WorkTask(), 5000, 3000);
		
		
	}
	
	public void AlbaInfoPrint(String subject) {
		txtAr.setText(subject + "를 해야지!");	
	}
	
	
	public void DelayTime(int sec) {
		try {
			Thread.sleep(sec);
		} catch (InterruptedException e) {}
	}

	
	class StudyCoading implements ActionListener { //코딩을 하였다.
		public void actionPerformed(ActionEvent e5) {
	
			if(timerCheck==300) {
				
				txtAr.setText("좋은 실력이였다.");
				DelayTime(3000);
				
				timerCheck++;
			} else if(timerCheck==301) {
				JOptionPane.showMessageDialog(null, "해당용어를 잘 배웠습니다.", "배움완료", JOptionPane.INFORMATION_MESSAGE);
				txtAr.setText("아직 일정이 남아있어! 무엇을 할까?\n (" + getThisDay() + "일째, " + getWorkTime() + "번 남음)");
				timerCheck=0;
				//hideStudyLogo();
				LRMenuShow();
				
			} else if(timerCheck==6) {
				
				
			}
			
			if(noMoneyCheck==100) {
				txtAr.setText("그런데..");
				noMoneyCheck++;
			} else if(noMoneyCheck==101) {
				txtAr.setText("그런데.. 돈이 부족하다..");
				noMoneyCheck++;
			} else if(noMoneyCheck==102) {
				txtAr.setText("다른걸 배워보자");
				LRMenuShow();
				noMoneyCheck=0;
			} 
			
			if(noMoneyCheck==200) {
				txtAr.setText("그런데..");
				noMoneyCheck++;
			} else if(noMoneyCheck==201) {
				txtAr.setText("그런데.. 다른 능력이 부족하다..");
				noMoneyCheck++;
			} else if(noMoneyCheck==202) {
				txtAr.setText("연관된 언어를 잘 생각해서 다른걸 먼저 배워보자");
				LRMenuShow();
				noMoneyCheck=0;
			} 	
			
			
			
		}
	}
*/



	

	
	public void LelvelInfoWindow() {
		
		StudyGoGo = new JPanel();
		StudyGoGo.setBackground(new Color(0, 0, 0, 50));
		StudyGoGo.setBounds(0, 0, 0, 0);
		frame.getContentPane().add(StudyGoGo);
		StudyGoGo.setLayout(null);
		
		JLabel lbStudyPhoto = new JLabel("New label");
		lbStudyPhoto.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/runStudy.gif")));
		lbStudyPhoto.setBounds(166, 65, 500, 307);
		StudyGoGo.add(lbStudyPhoto);
		
		StudyTextArea = new JTextArea();
		StudyTextArea.setEditable(false);
		StudyTextArea.setLineWrap(true);
		StudyTextArea.setBounds(166, 371, 500, 133);
		StudyGoGo.add(StudyTextArea);
		
	}
	
	public void ReLoadSellList() { //판매리스트 리로드
		txtrFf.setText("");
		txtrFf.append(isv.itemLoad_Win());
	}
	
	public int re_MyInfo(int ppis) {
		int resd=0;
		resd = ppis/4;
		
		if (resd < 0) {
			resd=0;
		}
		
		return resd;
	}
	public void myInfoView() { // 내정보
		dia1_myInfo=new JDialog();
		dia1_myInfo.setLocation(500, 200);
		dia1_myInfo.setResizable(false);
		dia1_myInfo.setSize(700, 574);
		dia1_myInfo.setVisible(true);
		JPanel levelInfos = new JPanel();
		levelInfos.setBounds(0, 0, 450, 642);
		
		levelInfos.setPreferredSize(new Dimension(700,574));
		dia1_myInfo.getContentPane().add(levelInfos);
		levelInfos.setLayout(null);
		
		JPanel MIpanel = new JPanel();
		MIpanel.setBounds(0, 0, 700, 574);
		levelInfos.add(MIpanel);
		MIpanel.setLayout(null);
		MIpanel.setPreferredSize(new Dimension(700,574));

		JPanel Html_statusbar = new JPanel();
		Html_statusbar.setBackground(new Color(255, 255, 255,0));
		Html_statusbar.setBounds(78, 86, 257, 10);
		MIpanel.add(Html_statusbar);
		Html_statusbar.setLayout(null);
		
		JPanel Html_Colorbar = new JPanel();
		Html_Colorbar.setBackground(new Color(234, 174, 129));
		Html_Colorbar.setBounds(0, 0, re_MyInfo(getHtml()), 10);
		Html_statusbar.add(Html_Colorbar);
		
		JLabel HtmlRealpoint = new JLabel(getHtml() + " Point");
		HtmlRealpoint.setForeground(UIManager.getColor("Button.disabledForeground"));
		HtmlRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		HtmlRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		HtmlRealpoint.setBounds(243, 98, 92, 15);
		MIpanel.add(HtmlRealpoint);
		
		JPanel Jquery_statusbar = new JPanel();
		Jquery_statusbar.setLayout(null);
		Jquery_statusbar.setBackground(new Color(255, 255, 255, 0));
		Jquery_statusbar.setBounds(425, 86, 257, 10);
		MIpanel.add(Jquery_statusbar);
		
		JPanel Jquery_Colorbar = new JPanel();
		Jquery_Colorbar.setBackground(new Color(154, 172, 204));
		Jquery_Colorbar.setBounds(0, 0, re_MyInfo(getJquery()), 10);
		Jquery_statusbar.add(Jquery_Colorbar);
		
		JLabel JqueryRealpoint = new JLabel(getJquery() + " Point");
		JqueryRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		JqueryRealpoint.setForeground(SystemColor.textInactiveText);
		JqueryRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		JqueryRealpoint.setBounds(590, 98, 92, 15);
		MIpanel.add(JqueryRealpoint);
		
		JPanel php_statusbar = new JPanel();
		php_statusbar.setLayout(null);
		php_statusbar.setBackground(new Color(255, 255, 255, 0));
		php_statusbar.setBounds(78, 137, 257, 10);
		MIpanel.add(php_statusbar);
		
		JPanel php_Colorbar = new JPanel();
		php_Colorbar.setBackground(new Color(165, 185, 140));
		php_Colorbar.setBounds(0, 0, re_MyInfo(getPhp()), 10);
		php_statusbar.add(php_Colorbar);
		
		JLabel phpRealpoint = new JLabel(getPhp() + " Point");
		phpRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		phpRealpoint.setForeground(SystemColor.textInactiveText);
		phpRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		phpRealpoint.setBounds(243, 149, 92, 15);
		MIpanel.add(phpRealpoint);
		
		JPanel asp_statusbar = new JPanel();
		asp_statusbar.setLayout(null);
		asp_statusbar.setBackground(new Color(255, 255, 255, 0));
		asp_statusbar.setBounds(425, 137, 257, 10);
		MIpanel.add(asp_statusbar);
		
		JPanel asp_Colorbar = new JPanel();
		asp_Colorbar.setBackground(new Color(149, 128, 187));
		asp_Colorbar.setBounds(0, 0, re_MyInfo(getAsp()), 10);
		asp_statusbar.add(asp_Colorbar);
		
		JLabel AspRealpoint = new JLabel(getAsp() + " Point");
		AspRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		AspRealpoint.setForeground(SystemColor.textInactiveText);
		AspRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		AspRealpoint.setBounds(590, 149, 92, 15);
		MIpanel.add(AspRealpoint);
		
		JPanel java_statusbar = new JPanel();
		java_statusbar.setLayout(null);
		java_statusbar.setBackground(new Color(255, 255, 255, 0));
		java_statusbar.setBounds(78, 188, 257, 10);
		MIpanel.add(java_statusbar);
		
		JPanel java_Colorbar = new JPanel();
		java_Colorbar.setBackground(new Color(146, 202, 205));
		java_Colorbar.setBounds(0, 0, re_MyInfo(getJava()), 10);
		java_statusbar.add(java_Colorbar);
		
		JLabel JavaRealpoint = new JLabel(getJava() + " Point");
		JavaRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		JavaRealpoint.setForeground(SystemColor.textInactiveText);
		JavaRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		JavaRealpoint.setBounds(243, 200, 92, 15);
		MIpanel.add(JavaRealpoint);
		
		JPanel jsp_statusbar = new JPanel();
		jsp_statusbar.setLayout(null);
		jsp_statusbar.setBackground(new Color(255, 255, 255, 0));
		jsp_statusbar.setBounds(425, 188, 257, 10);
		MIpanel.add(jsp_statusbar);
		
		JPanel jsp_Colorbar = new JPanel();
		jsp_Colorbar.setBackground(new Color(202, 139, 158));
		jsp_Colorbar.setBounds(0, 0, re_MyInfo(getJsp()), 10);
		jsp_statusbar.add(jsp_Colorbar);
		
		JLabel JSPRealpoint = new JLabel(getJsp() + " Point");
		JSPRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		JSPRealpoint.setForeground(SystemColor.textInactiveText);
		JSPRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		JSPRealpoint.setBounds(590, 200, 92, 15);
		MIpanel.add(JSPRealpoint);
		
		JPanel clang_statusbar = new JPanel();
		clang_statusbar.setLayout(null);
		clang_statusbar.setBackground(new Color(255, 255, 255, 0));
		clang_statusbar.setBounds(78, 239, 257, 10);
		MIpanel.add(clang_statusbar);
		
		JPanel clang_Colorbar = new JPanel();
		clang_Colorbar.setBackground(new Color(239, 122, 130));
		clang_Colorbar.setBounds(0, 0, re_MyInfo(getClang()), 10);
		clang_statusbar.add(clang_Colorbar);
		
		JLabel ClangRealpoint = new JLabel(getClang() + " Point");
		ClangRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		ClangRealpoint.setForeground(SystemColor.textInactiveText);
		ClangRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		ClangRealpoint.setBounds(243, 251, 92, 15);
		MIpanel.add(ClangRealpoint);
		
		JPanel cpp_statusbar = new JPanel();
		cpp_statusbar.setLayout(null);
		cpp_statusbar.setBackground(new Color(255, 255, 255, 0));
		cpp_statusbar.setBounds(425, 239, 257, 10);
		MIpanel.add(cpp_statusbar);
		
		JPanel cpp_Colorbar = new JPanel();
		cpp_Colorbar.setBackground(new Color(161, 202, 228));
		cpp_Colorbar.setBounds(0, 0, re_MyInfo(getCpp()), 10);
		cpp_statusbar.add(cpp_Colorbar);
		
		JLabel CppRealpoint = new JLabel(getCpp() + " Point");
		CppRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		CppRealpoint.setForeground(SystemColor.textInactiveText);
		CppRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		CppRealpoint.setBounds(590, 251, 92, 15);
		MIpanel.add(CppRealpoint);
		
		JPanel cshop_statusbar = new JPanel();
		cshop_statusbar.setLayout(null);
		cshop_statusbar.setBackground(new Color(255, 255, 255, 0));
		cshop_statusbar.setBounds(78, 291, 257, 10);
		MIpanel.add(cshop_statusbar);
		
		JPanel cshop_Colorbar = new JPanel();
		cshop_Colorbar.setBackground(new Color(123, 195, 152));
		cshop_Colorbar.setBounds(0, 0, re_MyInfo(getCshop()), 10);
		cshop_statusbar.add(cshop_Colorbar);
		
		JLabel CshopRealpoint = new JLabel(getCshop() + " Point");
		CshopRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		CshopRealpoint.setForeground(SystemColor.textInactiveText);
		CshopRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		CshopRealpoint.setBounds(243, 304, 92, 15);
		MIpanel.add(CshopRealpoint);
		
		JPanel python_statusbar = new JPanel();
		python_statusbar.setLayout(null);
		python_statusbar.setBackground(new Color(255, 255, 255, 0));
		python_statusbar.setBounds(425, 291, 257, 10);
		MIpanel.add(python_statusbar);
		
		JPanel python_Colorbar = new JPanel();
		python_Colorbar.setBackground(new Color(45, 90, 172));
		python_Colorbar.setBounds(0, 0, re_MyInfo(getPython()), 10);
		python_statusbar.add(python_Colorbar);
		
		JLabel PythonRealpoint = new JLabel(getPython() + " Point");
		PythonRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		PythonRealpoint.setForeground(SystemColor.textInactiveText);
		PythonRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		PythonRealpoint.setBounds(590, 304, 92, 15);
		MIpanel.add(PythonRealpoint);
		
		JPanel objc_statusbar = new JPanel();
		objc_statusbar.setLayout(null);
		objc_statusbar.setBackground(new Color(255, 255, 255, 0));
		objc_statusbar.setBounds(78, 342, 257, 10);
		MIpanel.add(objc_statusbar);
		
		JPanel objc_Colorbar = new JPanel();
		objc_Colorbar.setBackground(new Color(113, 113, 195));
		objc_Colorbar.setBounds(0, 0, re_MyInfo(getObject_c()), 10);
		objc_statusbar.add(objc_Colorbar);
		
		JLabel ObjCpoint = new JLabel(getObject_c() + " Point");
		ObjCpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		ObjCpoint.setForeground(SystemColor.textInactiveText);
		ObjCpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		ObjCpoint.setBounds(243, 355, 92, 15);
		MIpanel.add(ObjCpoint);
		
		JPanel swift_statusbar = new JPanel();
		swift_statusbar.setLayout(null);
		swift_statusbar.setBackground(new Color(255, 255, 255, 0));
		swift_statusbar.setBounds(425, 342, 257, 10);
		MIpanel.add(swift_statusbar);
		
		JPanel swift_Colorbar = new JPanel();
		swift_Colorbar.setBackground(new Color(156, 156, 198));
		swift_Colorbar.setBounds(0, 0, re_MyInfo(getSwift()), 10);
		swift_statusbar.add(swift_Colorbar);
		
		JLabel SwiftRealpoint = new JLabel(getSwift() + " Point");
		SwiftRealpoint.setHorizontalAlignment(SwingConstants.RIGHT);
		SwiftRealpoint.setForeground(SystemColor.textInactiveText);
		SwiftRealpoint.setFont(new Font("Arial", Font.ITALIC, 13));
		SwiftRealpoint.setBounds(590, 355, 92, 15);
		MIpanel.add(SwiftRealpoint);
		
		JLabel MyRankBg = new JLabel("");
		MyRankBg.setBounds(0, 0, 700, 574);
		MIpanel.add(MyRankBg);
		MyRankBg.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/RankMy.jpg")));
	}
	
	public void nameSetting() {
		
		dia1_name=new JDialog();
		dia1_name.setLocation(500, 200);
		dia1_name.setResizable(false);
		dia1_name.setSize(300, 400);
		dia1_name.setVisible(true);


		JLabel lblYourname = new JLabel("이름 입력 : ");
		lblYourname.setBounds(21, 63, 90, 24);
		dia1_name.getContentPane().add(lblYourname);
		lblYourname.setFont(new Font("맑은 고딕", Font.PLAIN, 17));
		
		textName = new JTextField();
		textName.setBounds(21, 97, 120, 45);
		dia1_name.getContentPane().add(textName);
		textName.setText("Kim");
		textName.setColumns(10);
		
		textAge = new JTextField();
		textAge.setBounds(160, 97, 120, 45);
		dia1_name.getContentPane().add(textAge);
		textAge.setText("23");
		textAge.setColumns(10);
		
		JLabel lblYourAge = new JLabel("나이 입력 :");
		lblYourAge.setBounds(160, 63, 84, 24);
		dia1_name.getContentPane().add(lblYourAge);
		lblYourAge.setFont(new Font("맑은 고딕", Font.PLAIN, 17));
		
		textJender = new JTextField();
		textJender.setBounds(21, 181, 120, 45);
		dia1_name.getContentPane().add(textJender);
		textJender.setText("1");
		textJender.setColumns(10);
		
		JLabel lblJender = new JLabel("성별코드 입력 : ( 1:남성 / 2:여성 )");
		lblJender.setBounds(21, 152, 259, 24);
		dia1_name.getContentPane().add(lblJender);
		lblJender.setFont(new Font("맑은 고딕", Font.PLAIN, 17));
		
		JButton btnConfirm = new JButton("확인");
		btnConfirm.setBounds(295, 213, 113, 66);
		dia1_name.getContentPane().add(btnConfirm);
		
		JLabel labelNotice = new JLabel("정보는 기본값입니다. 마음에 드는것으로 바꾸세요");
		labelNotice.setBounds(21, 22, 317, 20);
		dia1_name.getContentPane().add(labelNotice);
		labelNotice.setFont(new Font("맑은 고딕", Font.PLAIN, 14));
		
		JLabel lblBackground = new JLabel("");
		//lblBackground.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/ba.jpg")));
		lblBackground.setPreferredSize(new Dimension(420,300));
		lblBackground.setBounds(0, 0, 420, 301);
		dia1_name.getContentPane().add(lblBackground);
		

	
		btnConfirm.addActionListener(new ActionListener() {
		
			
			public void actionPerformed(ActionEvent e) {
				int age = 0, jender = 0;
				int ConfrimChk = 0;
				
				String realJender="";
					try {
						age = Integer.parseInt(textAge.getText());
					} catch (NumberFormatException e1) {
						JOptionPane.showMessageDialog(null, ment.opErrMsgWindow(1));
						age = 35628908;
					}
					
					if (age>=60 && age<=999) {
						JOptionPane.showMessageDialog(null, ment.opErrMsgWindow(3));
						ment.opErrMsg(3);
						
					} else if(age<=16) { 
						
						JOptionPane.showMessageDialog(null, ment.opErrMsgWindow(4));
						ment.opErrMsg(4);
						
					} else if(age<=60 && age>=17) { 
						yd.setAge(age);
						yd.setName(textName.getText());
						ConfrimChk+=1;
						
					} else if (age == 35628908) {
						
					} else {
						JOptionPane.showMessageDialog(null, ment.opErrMsgWindow(2));
						ment.opErrMsg(2);
					} 
				
				
				try {
					jender=Integer.parseInt(textJender.getText());
				} catch (NumberFormatException e2) {
					ment.opErrMsg(1);
					jender = 35628908;
				} catch (Exception e2) {}
				
					if (jender==1) {
						realJender = "남자";
						lblNewLabel.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/men_basic.png")));
						ConfrimChk+=1;
					} else if (jender==2) {
						realJender = "여자";
						lblNewLabel.setIcon(new ImageIcon(_GUIApp.class.getResource("/_NewGames/img/women_basic.png")));
						ConfrimChk+=1;
					} else if (jender==35628908) {
						System.out.print(" ");
					} else {
						ment.opErrMsg(2);
						System.out.print(" ");
					}

					String nameSet=textName.getText();
					nameSet = nameSet.replaceAll("\\s+","");
					nameSet = nameSet.replaceAll("-","");
					nameSet = nameSet.replaceAll("_","");
				
					if (nameSet.equals("안광식") || nameSet.equals("안강식") || nameSet.equals("안광심") || nameSet.equals("앤광식") || nameSet.equals("안광실") || nameSet.equals("광식안") || nameSet.equals("안갱식") || nameSet.equals("안꽝") || nameSet.equals("안광") || nameSet.equals("안광식 ") || nameSet.equals("안광씩") || nameSet.equals("안공식") || nameSet.equals("안구왕식") || nameSet.equals("안고왕식") || nameSet.equals("식광안") || nameSet.equals("광씩안")) {
						JOptionPane.showMessageDialog(null, "사용할수 없는 이름입니다.");
					} else if (nameSet.equals("광식")||nameSet.equals("강식")||nameSet.equals("식광")||nameSet.equals("식광이")||nameSet.equals("광식이")||nameSet.equals("광식님")) {
						JOptionPane.showMessageDialog(null, "이것도 막아 놨습니다.");
					}else if (nameSet.equals("AhnGwangShik") || nameSet.equals("AnGwangShik") || nameSet.equals("AhnGwangSik") || nameSet.equals("AnKwangShik") || nameSet.equals("AhnGwnag") || nameSet.equals("AhnS.K") || nameSet.equals("ahngwangshik") || nameSet.equals("AhnGwang-Sik") || nameSet.equals("AnGwang-Shik") || nameSet.equals("AnGwang-Sik") || nameSet.equals("Gwang-Shik") || nameSet.equals("AnGwangSik")) {
						JOptionPane.showMessageDialog(null, "영어로도 안됩니다.");
					}else {
						ConfrimChk+=1;
					}
					
				
				if (ConfrimChk>=3) {
					yd.BasicSet(textName.getText(), realJender, age);
					dia1_name.dispose();
					txtAr.setText("안녕!! " + yd.getAge() + "살이고, " + yd.getJender() + "인, " + yd.getName() + "!!");
					lbl_MyName.setText( yd.getName() + "(" + yd.getAge() + ")");
					
					yd.setMoney(ym.ageRandMoney());
					
					if (getName().equals("ShowMoney")) {
						yd.setMoney(999999);
						JOptionPane.showMessageDialog(null, "치트발동! 999999원!");
					}
					
					if (getName().equals("MasterHtml")) {
						yd.setHtml(800);
						JOptionPane.showMessageDialog(null, "치트발동!! Html마스터");
					}
					
					if (getName().equals("FreeMania")) {
						yd.setHtmlMoney(0);
						yd.setJqueryMoney(0);
						yd.setAspMoney(0);
						yd.setPhpMoney(0);		
						yd.setJspMoney(0);
						yd.setJavaMoney(0);
						yd.setClangMoney(0);
						yd.setCppMoney(0);
						yd.setCshopMoney(0);
						yd.setObject_cMoney(0);
						yd.setPythonMoney(0);
						yd.setSwiftMoney(0);
						JOptionPane.showMessageDialog(null, "치트발동!! 모든강의 공짜");
					}
					opCheck = 5;
				} else {
					JOptionPane.showMessageDialog(null, ment.opErrMsgWindow(2));
				}
				
				
				
			}
		});
		
		
	}
}

