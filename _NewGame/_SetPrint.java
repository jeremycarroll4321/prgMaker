﻿package _NewGame;

public class _SetPrint extends _GUIApp {
	
	
	//엔터포함 문자출력
	public static void MSG(String msg, int Sec) {
		
		System.out.println(msg);
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}
		
	}
	
	//엔터없이 문자출력
	public static void MSGL(String msg, int Sec) {
		System.out.print(msg);
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}
	}	
	
	
	//한글자씩 출력 - 타이틀,메뉴전환용
	public static void Titles(String msg, int Sec) {
		
		String lineDraw = "==============================";
		System.out.println("\n\n\n");
	//	Sec = 1;
		
	    for (int i = 0; i < lineDraw.length(); i++) {
	        System.out.print( lineDraw.charAt(i) );
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {}
	      }
	    
	    System.out.println("");
	    
	    for (int i = 0; i < msg.length(); i++) {
	        System.out.print( msg.charAt(i) );
			try {
				Thread.sleep(Sec);
			} catch (InterruptedException e) {}
	      }
	    
	    System.out.println("");
	    
	    for (int i = 0; i < lineDraw.length(); i++) {
	        System.out.print( lineDraw.charAt(i) );
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {}
	      }	    
	    System.out.println("\n\n\n");
	}		
	
	
	public static void Titles2(String msg, int Sec) {
		
		String lineDraw = "==============================";
		System.out.println("\n\n\n");
	//	Sec = 1;
		
	    for (int i = 0; i < lineDraw.length(); i++) {
	        System.out.print( lineDraw.charAt(i) );
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {}
	      }
	    
	    System.out.println("");
	    
	    for (int i = 0; i < msg.length(); i++) {
	        System.out.print( msg.charAt(i) );
			try {
				Thread.sleep(Sec);
			} catch (InterruptedException e) {}
	      }
	    
	    System.out.println("");
	    
	    for (int i = 0; i < lineDraw.length(); i++) {
	        System.out.print( lineDraw.charAt(i) );
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {}
	      }	    
	    System.out.println("");
	}		
	
	//로딩용
	public static void Studing(String msg, int Sec) {
		System.out.print(msg);
	//	Sec = 1;
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}
		
		System.out.print(".");
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}		
		System.out.print(".");
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}		
		System.out.print(".");
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}		
		System.out.print(".");
		try {
			Thread.sleep(Sec);
		} catch (InterruptedException e) {}		
		System.out.print("\n");
	}	
	
	//날짜
	public static void dayPrint(String msg, int Sec) {
		System.out.println("\n\n");
		String dayPrinting = "[" + msg + "]";

		
	    for (int i = 0; i < dayPrinting.length(); i++) {
	        System.out.print( dayPrinting.charAt(i) );
			try {
				Thread.sleep(Sec);
			} catch (InterruptedException e) {}
	      }
	
	}		
	
	
	
}
