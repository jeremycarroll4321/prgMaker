﻿package _NewGame;
import java.util.Random;

public class Lotto extends YourData {
	
	public void useMoney(int mMoney) {
		int money = getMoney();
		money -= mMoney;
		setMoney(money);	
	}
	public void plusMoney(int pMoney) {
		System.out.println();
		int money = getMoney();
		money += pMoney;
		setMoney(money);	
		_SetPrint.MSG("현재 잔액 : " + getMoney(), 600);
	
	}
	
	public void plusHappy(int hPoint) {
		_SetPrint.MSG("당첨이 크게 됐다! ", 600);
		int happy = getHappy();
		happy += hPoint;
		setHappy(happy);	
		_SetPrint.MSG("현재 기분 : " + getHappy(), 600);
		System.out.println();
	}
	
	
	public void minusHappy(int hPoint) {
		_SetPrint.MSG("돈을 잃었다..", 600);
		int happy = getHappy();
		happy -= hPoint;
		setHappy(happy);	
		_SetPrint.MSG("현재 기분 : " + getHappy(), 600);
		System.out.println();
	}	
	
	public void BasicHappy() {
		System.out.println();
	}	
	
	public void noMoney() {
		System.out.println();
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("꽝!!", 600);
		_SetPrint.MSG("현재 잔액 : " + getMoney(), 600);
		minusHappy(7);
		System.out.println();
	}
	
	public void Money20() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("20원!", 600);
		plusMoney(20);
		minusHappy(4);
	}	
	
	public void Money60() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("60원!", 600);
		plusMoney(60);
		minusHappy(2);
	}	
	
	public void Money80() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("80원!", 600);
		plusMoney(80);
		minusHappy(1);
	}		
	
	public void Money100() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("100원!", 600);
		plusMoney(100);
		BasicHappy();
	}		
	public void Money500() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("500원!", 600);
		plusMoney(100);
		plusHappy(20);
	}	
	public void Money800() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("800원!", 600);
		plusMoney(800);
		plusHappy(25);
	}	
	public void Money1000() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("1,000원!", 600);
		plusMoney(1000);
		plusHappy(30);
	}	
	public void Money1200() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("1,200원!", 600);
		plusMoney(1200);
		plusHappy(33);
	}	
	public void Money1500() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("1,500원!", 600);
		plusMoney(1500);
		plusHappy(35);
	}	
	public void Money2000() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("2,000원!", 600);
		plusMoney(2000);
		plusHappy(40);
	}	
	public void Money2500() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("2,500원!", 600);
		plusMoney(2500);
		plusHappy(50);
	}		
	public void Money10000() {
		_SetPrint.Studing("복권 긁는중", 600);
		_SetPrint.MSG("************10,000원!*************", 600);
		plusMoney(10000);
		plusHappy(200);
	}	
	
	public void ev_start() {
		useMoney(100);
		
		Random randomGenerator = new Random();
		
		RandomEvent randEv = new RandomEvent();
		int startpoint = 1;
		int endpoint = 130;
		double rangepoint = endpoint - startpoint + 1;
		int randGoOut = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		startpoint = 1;
		endpoint = 25;
		rangepoint = endpoint - startpoint + 1;
		int randGoOut2 = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		if (randGoOut==3 || randGoOut==6 ||randGoOut==12 || randGoOut==22 || randGoOut==31) {
			Money20();
		} else if (randGoOut==4 || randGoOut==7 ||randGoOut==13 || randGoOut==23 || randGoOut==32 || randGoOut==53 ||randGoOut==76 ) {
			Money60();
		} else if (randGoOut==5 || randGoOut==8 ||randGoOut==14 || randGoOut==24 || randGoOut==33 || randGoOut==54 || randGoOut==69 ||randGoOut==79 || randGoOut==96 || randGoOut==103 || randGoOut==52 ||randGoOut==70 || randGoOut==82 || randGoOut==92 || randGoOut==115 || randGoOut==83 || randGoOut==95 || randGoOut==117) {
			Money80();
		} else if (randGoOut==11 || randGoOut==5 || randGoOut==25 || randGoOut==16 || randGoOut==32 || randGoOut==45 ||randGoOut==62 || randGoOut==68 || randGoOut==27 || randGoOut==72 || randGoOut==55 || randGoOut==56 || randGoOut==80 || randGoOut==86 || randGoOut==99 || randGoOut==103 || randGoOut==102 ||randGoOut==41 ||randGoOut==53 ||randGoOut==68 ||randGoOut==72) {
			Money100();
		} else if (randGoOut==15 || randGoOut==33 || randGoOut==46 ||randGoOut==63 ||randGoOut==78) {
			Money500();
		} else if (randGoOut==16 || randGoOut==34 || randGoOut==47 ||randGoOut==64) {
			Money800();
		} else if (randGoOut==17 || randGoOut==35 || randGoOut==48) {
			Money1000();
		} else if (randGoOut==19 || randGoOut==21) {
			if (randGoOut2 > 14) {
				Money1500();
			} else {
				Money1200();
			}
		} else if (randGoOut==77 ) {
			if (randGoOut2==7) {
				Money10000();
			} else {
				Money2000();
			}
		} else {
			noMoney();
		}
	}
}
