﻿package _NewGame;

import java.util.Random;

public class SelectWorkMenu extends YourData {

	YourData yd = new YourData();
	
	public SelectWorkMenu() {
		super();
	}
	
	public void todayInfo() {
		String str = getThisDay() + "일째 / " + getYoil();
		
		_SetPrint.dayPrint(str,40);
	}
	
	public static void helloMessage() {
		Random hemsg = new Random();
		if (getHappy() >= 160) {

			// 행복도 160이상
			String[] hmessage = {"요즘 너무 행복해", 
								"코딩을 한다는건 너무 즐거운 일이야", 
								"오늘도 코딩 내일도 코딩", 
								"나는야 코딩이 좋아",
								"코딩을 해서 하루하루가 행복해",
								"오늘은 정말 행복한 날이야",
								"요새 행복한 하루가 계속되서 가끔 걱정이야", 
								"이 행복이 끝까지 갔으면 좋겠다", 
								"난 행복해! 넌 어때?",
								"나는 정말 좋아!",
								"기쁜하루가 계속되서 너무 좋아"};
			for (int i=0; i<1; i++)
				_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				System.out.println();
				
		} else if (getHappy() >= 40) {
			
			// 행복도 40이상
			String[] hmessage = {"오늘도 열심히 배워보자", 
								"오늘 하루도 즐겁게 배울 수 있을거야", 
								"난 잘 배우고 있어! 너도 그렇지?", 
								"난 잘 할 수 있을거 같은 기분이 들어",
								"행복한 하루를 보낼 수 있을거야",
								"즐겁게 오늘 하루를 보내야지",
								"오늘은 무슨일을 할까", 
								"오늘은 어떤 즐거운 일을하지?", 
								"행복한 하루가 될꺼야!",
								"즐거운 하루가 될꺼야!",
								"오늘도 잘 배워보자"};
			for (int i=0; i<1; i++)
				_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				System.out.println();
				
		} else {
			// 그 이하
			String[] hmessage = {"코딩이 좀 더 필요해...", 
								"너무 슬퍼..", 
								"나 요새 좀 우울해", 
								"나 요즘 너무 힘든데.. 너는?",
								"그만 하고싶다..",
								"우울감이 너무 싫어",
								"코딩을 배우고 싶어..", 
								"너무 힘든 하루가 계속되는거 같아", 
								"앞으로의 일정이 두려운걸",
								"힘들다..",
								"우울해.."};
			for (int i=0; i<1; i++)
				_SetPrint.MSG(hmessage[hemsg.nextInt(11)], 1000);
				System.out.println();
		}
		

	}

	public void dayHellow() {
		todayInfo(); //며칠째인지 출력
		_SetPrint.MSG(" ", 1000);
		helloMessage(); //헬로 메세지 출력
	}
	
	
	
	public void menuList() {
		System.out.println("====Menu=====");
		System.out.println("1. 일정잡기");
		System.out.println("2. 현재 정보");
		System.out.println("3. 아이템샵");		
		System.out.println("4. 종료");
		System.out.println("=============");
	}
	
	
	public void tutorial() {
		_SetPrint.Titles2("~~초기 설정 안내~~", 20);
		_SetPrint.MSG("메뉴를 선택해서 일정을 잡을 수 있고", 800);
		_SetPrint.MSG("현재 정보에서는 내 경험치를 볼 수 있어!", 800);
		_SetPrint.MSG("아이템샵에선 다양한 아이템으로 경험치를 더 빨리 늘릴 수 있습니다.", 800);
		_SetPrint.MSG("또한 하루에 한번 씩 모든 프로그래밍 능력치가 3점씩 감소합니다.", 800);
	}
	
	public void basicInfoPrint() {
		System.out.println("\n======================");
		System.out.println("이름 : " + getName());
		System.out.println("자금 : " + getMoney());
		System.out.println("기분 : " + getHappy());
		System.out.println("======================");
	}
	
	public void menuSelect() {

			//엔딩체크
			EndingCecker edc = new EndingCecker();
			edc.chk();
			
			//매일매일 -3점씩 감소
			AllMinus am = new AllMinus();
			am.AllminusData(3);
			int minusData = getAlba();
			minusData -= 3;
			setAlba(minusData);
			
			//능력치 맥스값 체크
			MaxChk mchk = new MaxChk();
			mchk.MaxChkList();
			
			//날짜체크
			DayChk dck = new DayChk();
			dck.days();
			

			
			
			dayHellow();
			basicInfoPrint(); //Info출력
			menuList();
			int menusel = 0;
			System.out.print("선택 : ");
			menusel = Integer.parseInt(ReadValue.input());
			
			while(true) {
				
				switch (menusel) {
				case 1: //일정선택
					setWorkTime(4); //4회까지 가능
					SelectToday st = new SelectToday();
					st.menuPrint();
					break;
				case 2:
					YourStudyData sw = new YourStudyData();
					sw.ShowData();
					break;
				case 3:
					ItemStore it = new ItemStore();
					it.start();
				default:
					menuSelect();
					break;
				}
			}
		
		
		
	}
	
}
