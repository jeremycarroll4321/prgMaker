﻿package _NewGame;


public class ItemSaver extends YourData{
	ItemInfo iinfo = new ItemInfo();
	private ItemInfo ist[];//int score[];
	private int count;
	
	public ItemSaver() {
		ist=new ItemInfo[15];
	}
	
	
	public void itemSave(ItemInfo itemFo) {
        if(count<ist.length)
        	ist[count++]=itemFo;
           else
           System.out.println("저장 공간이 부족해!!");
	}

	
	public void itemLoad() {
		for (int i=0; i<ist.length; i++) {
			_SetPrint.MSG(ist[i] + "", 30);
		}
	}
	
	public String itemLoad_Win() {
		String str="";
		for (int i=0; i<ist.length; i++) {
			str += ist[i] + "\n";
		}
		return str;
	}
	
	public void itemDay_Event12() { //대출체크
	
		int itemNum = 12;
		for (int i=0; i<count; i++) {
			int pname=ist[i].getItemNumber(); //이름을 찾는다.
			if (itemNum == pname) { // 만일 검색한 이름과 정보가 같다면
				_SetPrint.MSG("오늘" + getDaechulDayMoney() + "원 가져갑니다.", 600);
				int nowMoney = getDachulAllMoney();
				nowMoney -= getDaechulDayMoney();
				setMoney(nowMoney);
				_SetPrint.MSG("현재 대출액은 " + getDachulAllMoney() + "원 남았습니다.", 600);
			}
		}
	}
	
	public void itemDel(int itemNum) {
		
		for (int i=0; i<count; i++) {
			int pname=ist[i].getItemNumber(); //이름을 찾는다.
			if (itemNum==pname) { // 만일 검색한 이름과 정보가 같다면
				for(int j=i; j<count; j++) {//해당사람 정보 삭제
					ist[j]=ist[j+1]; //p->배열뜻함 j값->이름과 일치한 배열 j번째 / p[0]=p[1]이란 뜻 
				}
				count--; // 배열 수 의미
				iinfo.itemDelChecker(itemNum);
				_SetPrint.MSG("감사합니다!", 600);
			}
		}
	}
	
	
	
}
